package com.bestbets.app.configuration;

import com.bestbets.app.model.support.UserProfileType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class CustomSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    protected void handle(HttpServletRequest request,
                          HttpServletResponse response, Authentication authentication) throws IOException {
        String targetUrl = determineTargetUrl(authentication);
        if (response.isCommitted()) {
            System.out.println("Can't redirect");
            return;
        }

        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    protected String determineTargetUrl(Authentication authentication) {
        String url = "";

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        List<String> roles = new ArrayList<String>();

        for (GrantedAuthority a : authorities) {
            roles.add(a.getAuthority());
        }

        if (isModerator(roles)) {
            url = "/moderator";
        } else if (isAdmin(roles)) {
            url = "/#/admin";
        } else if (isAnalitic(roles)) {
            url = "/#/analytic";
        } else if (isUser(roles)) {
            url = "/user";
        } else {
            url = "/accessDenied";
        }

        return url;
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }

    private boolean isUser(List<String> roles) {
        if (roles.contains("ROLE_"+ UserProfileType.USER.getUserProfileType())) {
            return true;
        }
        return false;
    }

    private boolean isAdmin(List<String> roles) {
        if (roles.contains("ROLE_"+UserProfileType.ADMIN.getUserProfileType())) {
            return true;
        }
        return false;
    }

    private boolean isModerator(List<String> roles) {
        if (roles.contains("ROLE_" + UserProfileType.MODERATOR.getUserProfileType())) {
            return true;
        }
        return false;
    }

    private boolean isAnalitic(List<String> roles) {
        if (roles.contains("ROLE_"+ UserProfileType.ANALYTIC.getUserProfileType())) {
            return true;
        }
        return false;
    }

}