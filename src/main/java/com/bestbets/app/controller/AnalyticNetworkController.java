package com.bestbets.app.controller;

import com.bestbets.app.model.Sport;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.service.AnalyticNetworkService;
import com.bestbets.app.service.SportEventService;
import com.bestbets.app.service.SportService;
import com.bestbets.app.service.managers.AnalyticManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/analyticnetwork")
public class AnalyticNetworkController {
    @Autowired
    AnalyticNetworkService analyticNetworkService;
    @Autowired
    AnalyticManager analyticManager;
    @Autowired
    SportEventService sportEventService;
    @Autowired
    SportService sportService;

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<List<Double>> calculateSEParametrs(@PathVariable("id") Integer id){
        SportEvent sportEvent = sportEventService.findById(id);
        List<Double> doubles = analyticManager.parametersForView(sportEvent);
        return new ResponseEntity<List<Double>>(doubles,HttpStatus.OK);
    }

    @RequestMapping(value = "/predictsport/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> createNeuralNet(@PathVariable("id") Integer id){
        Sport sport = sportService.findById(id);
        analyticManager.createNeuralNetworkBySport(sport);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
