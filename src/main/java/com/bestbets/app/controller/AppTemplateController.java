package com.bestbets.app.controller;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AppTemplateController {

    @RequestMapping(value = {"/login"},method = RequestMethod.GET)
    public String getIndexPage() {
        return "template/login";
    }

    @RequestMapping(value = {"/registration"}, method = RequestMethod.GET)
    public String getRegistrationTemplate() {
        return "template/registration";
    }

    @RequestMapping(value = {"/admin"}, method = RequestMethod.GET)
    public String getAdminTemplate(ModelMap model){ UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("userMail",principal.getUsername());
        return "template/admin";}
    @RequestMapping(value = {"/analytic"}, method = RequestMethod.GET)
    public String getAnalyticTemplate(ModelMap model){
        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("userMail",principal.getUsername());
        return "template/analytic";
    }

    @RequestMapping(value = {"/sports"}, method = RequestMethod.GET)
    public String getSportList() {
        return "template/sport_list";
    }

    @RequestMapping(value = {"/tournaments"}, method = RequestMethod.GET)
    public String getTournamentList() {
        return "template/tournament_list";
    }

    @RequestMapping(value = {"/teams"}, method = RequestMethod.GET)
    public String getTeamsList() {
        return "template/team_list";
    }

    @RequestMapping(value = {"/sportevents"}, method = RequestMethod.GET)
    public String getSportEventsList() {
        return "template/sport_event_list";
    }
    @RequestMapping(value = {"/details"}, method = RequestMethod.GET)
    public String getDetailsTemplate(){
        return "template/details";
    }

   /* @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage() {
        return "/admin";
    }

    @RequestMapping(value = "/moderator", method = RequestMethod.GET)
    public String moderatorPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        List<Sport> sports = sportService.findAll();
        model.addAttribute("sports", sports);
        return "moderator/moderator";
    }
    @RequestMapping(value = "/analitic", method = RequestMethod.GET)
    public String analiticPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "analitic/analitic";
    }
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String userPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "user/user";
    }

    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        return "login";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    private String getPrincipal(){
        String email = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            email = ((UserDetails)principal).getUsername();
        } else {
            email = principal.toString();
        }
        return email;
    }*/
}