package com.bestbets.app.controller;

import com.bestbets.app.model.Bet;
import com.bestbets.app.model.User;
import com.bestbets.app.service.BetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/bet")
public class BetController {
    @Autowired
    BetService betService;

    @RequestMapping(value = {"/all"}, method = RequestMethod.GET)
    public ResponseEntity<List<Bet>> listBets(@RequestBody User user) {
        List<Bet> bets = betService.listBetsByUser(user);
        if (bets.isEmpty()) {
            return new ResponseEntity<List<Bet>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Bet>>(bets, HttpStatus.OK);
    }
    @RequestMapping(value = {"/currentbets/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<Bet>> listCurrentBets(@PathVariable User user) {
        List<Bet> bets = betService.currentBetsByUser(user);
        if (bets.isEmpty()) {
            return new ResponseEntity<List<Bet>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Bet>>(bets, HttpStatus.OK);
    }
    @RequestMapping(value = {"/historybets"}, method = RequestMethod.GET)
    public ResponseEntity<List<Bet>> listHistoryBets(@RequestBody User user) {
        List<Bet> bets = betService.historyBetsByUser(user);
        if (bets.isEmpty()) {
            return new ResponseEntity<List<Bet>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Bet>>(bets, HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Bet> getBet(@PathVariable("id") Integer id) {
        Bet bet = betService.findById(id);
        if (bet == null) {
            return new ResponseEntity<Bet>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Bet>(bet, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createBet(@RequestBody Bet bet, UriComponentsBuilder componentsBuilderl) {

        if (betService.isBetExist(bet)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        betService.save(bet);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(componentsBuilderl.path("/bet/{id}").buildAndExpand(bet.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/bet/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Bet> updateBet(@PathVariable("id") Integer id, @RequestBody Bet bet) {
        Bet currentBet = betService.findById(id);
        if (betService == null) {
            return new ResponseEntity<Bet>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Bet>(bet, HttpStatus.OK);
    }
}
