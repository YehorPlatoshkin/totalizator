package com.bestbets.app.controller;

import com.bestbets.app.model.BetMark;
import com.bestbets.app.service.BetMarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/betmark")
public class BetMarkController {
    @Autowired
    BetMarkService betMarkService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<BetMark>> getBetMarks(){
        List<BetMark> betMarks = betMarkService.listAll();
        if (betMarks.isEmpty()){
            return new ResponseEntity<List<BetMark>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<BetMark>>(betMarks,HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<BetMark> getBetMarkyId(@PathVariable("id") Integer id){
        BetMark betMark = betMarkService.findById(id);
        if (betMark==null){
            return new ResponseEntity<BetMark>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<BetMark>(betMark,HttpStatus.OK);
    }
}
