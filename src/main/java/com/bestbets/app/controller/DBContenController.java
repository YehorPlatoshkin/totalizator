package com.bestbets.app.controller;

import com.bestbets.app.service.DataBaseContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dbcontent")
public class DBContenController {
    @Autowired
    DataBaseContent dataBaseContent;
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Void> build(){
        dataBaseContent.build();
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/cs", method = RequestMethod.GET)
    public ResponseEntity<Void> createSport(){
        dataBaseContent.createSport();
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/cse", method = RequestMethod.GET)
    public ResponseEntity<Void> createSportEvents(){
        dataBaseContent.createSportEvents();
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
