package com.bestbets.app.controller;

import com.bestbets.app.model.EventCoefficient;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.service.EventCoefficientService;
import com.bestbets.app.service.SportEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/eventcoefficient")
public class EventCoefficientController {
    @Autowired
    EventCoefficientService eventCoefficientService;
    @Autowired
    SportEventService sportEventService;

    @RequestMapping(value = "/sportevent/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<EventCoefficient>> getCoefficientsBySportEvent(@PathVariable("id") Integer id){
        SportEvent sportEvent = sportEventService.findById(id);
        if (sportEvent == null){
            return  new ResponseEntity<List<EventCoefficient>>(HttpStatus.NOT_FOUND);
        }
        List<EventCoefficient> eventCoefficients = eventCoefficientService.findBySportEvent(sportEvent);
        if (eventCoefficients.isEmpty()){
            return new ResponseEntity<List<EventCoefficient>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<EventCoefficient>>(eventCoefficients,HttpStatus.OK);
    }

    @RequestMapping(value = "/calc/{id}", method = RequestMethod.POST)
    public ResponseEntity<Void> calculateCoefficientsBySportEvent(@PathVariable("id") Integer id){
        SportEvent sportEvent = sportEventService.findById(id);
        if (sportEvent == null){
            return  new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        if(!eventCoefficientService.findBySportEvent(sportEvent).isEmpty()){
            return  new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        eventCoefficientService.setEventCoefficientForSportEvent(sportEvent);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
