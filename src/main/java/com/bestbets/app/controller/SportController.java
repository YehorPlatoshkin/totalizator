package com.bestbets.app.controller;

import com.bestbets.app.model.Sport;
import com.bestbets.app.service.SportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/sport")
public class SportController {
    @Autowired
    SportService sportService;

    @RequestMapping(value = {"/all"}, method = RequestMethod.GET)
    public ResponseEntity<List<Sport>> listSports() {
        List<Sport> sports = sportService.findAll();
        if (sports.isEmpty()) {
            return new ResponseEntity<List<Sport>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Sport>>(sports, HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Sport> getSport(@PathVariable("id") Integer id) {
        Sport sport = sportService.findById(id);
        if (sport == null) {
            return new ResponseEntity<Sport>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Sport>(sport, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createSport(@RequestBody Sport sport, UriComponentsBuilder componentsBuilderl) {
        if (sportService.isExist(sport)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        sportService.create(sport);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(componentsBuilderl.path("/sport/{id}").buildAndExpand(sport.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Sport> updateSport(@RequestBody Sport sport) {
        Sport current = sportService.findById(sport.getId());
        if (current == null) {
            return new ResponseEntity<Sport>(HttpStatus.NOT_FOUND);
        }
        current.setSportName(sport.getSportName());
        sportService.update(current);
        return new ResponseEntity<Sport>(sport, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Sport> deleteSportById(@PathVariable("id") Integer id) {
        Sport user = sportService.findById(id);
        if (user == null) {
            return new ResponseEntity<Sport>(HttpStatus.NOT_FOUND);
        }
        sportService.delete(user);
        return new ResponseEntity<Sport>(HttpStatus.NO_CONTENT);
    }
}
