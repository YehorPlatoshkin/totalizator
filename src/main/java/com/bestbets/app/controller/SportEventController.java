package com.bestbets.app.controller;

import com.bestbets.app.model.Sport;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.model.TournamentsTeams;
import com.bestbets.app.service.SportEventService;
import com.bestbets.app.service.SportService;
import com.bestbets.app.service.TournamentService;
import com.bestbets.app.service.TournamentsTeamsService;
import com.bestbets.app.service.managers.SportEventManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
@RestController
@RequestMapping("/sportevent")
public class SportEventController {
    @Autowired
    SportService sportService;
    @Autowired
    TournamentService tournamentService;
    @Autowired
    TournamentsTeamsService tournamentsTeamsService;
    @Autowired
    SportEventService sportEventService;
    @Autowired
    SportEventManager eventManager;

    @RequestMapping(value = {"/sport/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Set<SportEvent>> listSportSportEvents(@PathVariable("id") Integer id) {
        Sport sport = sportService.findById(id);
        Set<SportEvent> sportEventsBySport = sportEventService.findAllBySport(sport);
        if (sportEventsBySport.isEmpty()) {
            return new ResponseEntity<Set<SportEvent>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Set<SportEvent>>(sportEventsBySport, HttpStatus.OK);
    }

    @RequestMapping(value = {"/tournament/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Set<SportEvent>> listTournamentSportEvents(@PathVariable("id") Integer id) {
        Tournament tournament = tournamentService.findById(id);
        Set<SportEvent> sportEventsByTournament = sportEventService.findAllByTournament(tournament);
        if (sportEventsByTournament.isEmpty()) {
            return new ResponseEntity<Set<SportEvent>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Set<SportEvent>>(sportEventsByTournament, HttpStatus.OK);
    }

    @RequestMapping(value = {"/team/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<SportEvent>> listTeamSportEvents(@PathVariable("id") Integer id) {
        TournamentsTeams teams = tournamentsTeamsService.singleTournamentTeamById(id);
        List<SportEvent> sportEventsByTeam = sportEventService.findAllByTeam(teams);
        if (sportEventsByTeam.isEmpty()) {
            return new ResponseEntity<List<SportEvent>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<SportEvent>>(sportEventsByTeam, HttpStatus.OK);
    }
    @RequestMapping(value = {"/pastEventsGT/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<SportEvent>> pastEventsGT(@PathVariable("id") Integer id){
        SportEvent currentSE = sportEventService.findById(id);
        List<SportEvent> pastEventsByTeam = sportEventService.findPastEventsByTeam(currentSE.getGuestTeam(), currentSE.getDateTime());
        if (pastEventsByTeam.isEmpty()){
            return new ResponseEntity<List<SportEvent>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<SportEvent>>(pastEventsByTeam, HttpStatus.OK);
    }
    @RequestMapping(value = {"/pastEventsHT/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<SportEvent>> pastEventsHT(@PathVariable("id") Integer id){
        SportEvent currentSE = sportEventService.findById(id);
        List<SportEvent> pastEventsByTeam = sportEventService.findPastEventsByTeam(currentSE.getHomeTeam(), currentSE.getDateTime());
        if (pastEventsByTeam.isEmpty()){
            return new ResponseEntity<List<SportEvent>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<SportEvent>>(pastEventsByTeam, HttpStatus.OK);
    }
    @RequestMapping(value = {"/currentsportevents"}, method = RequestMethod.GET)
    public ResponseEntity<List<SportEvent>> listCurrentSportEvents() {
        List<SportEvent> currentSportEventsByTournament = sportEventService.findCurrentEvents();
        if (currentSportEventsByTournament.isEmpty()) {
            return new ResponseEntity<List<SportEvent>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<SportEvent>>(currentSportEventsByTournament, HttpStatus.OK);
    }

    @RequestMapping(value = {"/historysportevents"}, method = RequestMethod.GET)
    public ResponseEntity<List<SportEvent>> listPastSportEvents() {
        List<SportEvent> pastSportEvents = sportEventService.findPastEvents();
        if (pastSportEvents.isEmpty()) {
            return new ResponseEntity<List<SportEvent>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<SportEvent>>(pastSportEvents, HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<SportEvent> getSportEvent(@PathVariable("id") Integer id) {
        SportEvent sportEvent = sportEventService.findById(id);
        if (sportEvent == null) {
            return new ResponseEntity<SportEvent>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<SportEvent>(sportEvent, HttpStatus.OK);
    }

    @RequestMapping( method = RequestMethod.POST)
    public ResponseEntity<Void> createSportEvent(@RequestBody SportEvent sportEvent/*, UriComponentsBuilder componentsBuilderl*/) {
        if (sportEventService.isExist(sportEvent)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        eventManager.openSportEvent(sportEvent);
        /*HttpHeaders headers = new HttpHeaders();
        headers.setLocation(componentsBuilderl.path("/").buildAndExpand(sportEvent.getId()).toUri());*/
        return new ResponseEntity<Void>( HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<SportEvent> updateSportEvent(@RequestBody SportEvent sportEvent) {
        SportEvent currentSportEvent = sportEventService.findById(sportEvent.getId());
        if (currentSportEvent == null) {
            return new ResponseEntity<SportEvent>(HttpStatus.NOT_FOUND);
        }
        currentSportEvent.setResultSet(sportEvent.getResultSet());
        currentSportEvent.setDateTime(sportEvent.getDateTime());
        currentSportEvent.setCoefficients(sportEvent.getCoefficients());
        currentSportEvent.setBetList(sportEvent.getBetList());
        currentSportEvent.setHomeTeam(sportEvent.getHomeTeam());
        currentSportEvent.setGuestTeam(sportEvent.getGuestTeam());
        sportEventService.update(currentSportEvent);
        return new ResponseEntity<SportEvent>(sportEvent, HttpStatus.OK);
    }

}
