package com.bestbets.app.controller;

import com.bestbets.app.model.Team;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.model.TournamentsTeams;
import com.bestbets.app.service.TeamService;
import com.bestbets.app.service.TournamentService;
import com.bestbets.app.service.TournamentsTeamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/team")
public class TeamController {
    @Autowired
    TeamService teamService;
    @Autowired
    TournamentService tournamentService;
    @Autowired
    TournamentsTeamsService tournamentsTeamsService;

    @RequestMapping(value = {"/bytournament/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<TournamentsTeams>> listSports(@PathVariable("id") Integer id ) {
        Tournament tournament = tournamentService.findById(id);
        List<TournamentsTeams> teams = tournamentsTeamsService.listTeamsByTournament(tournament);
        if (teams.isEmpty()) {
            return new ResponseEntity<List<TournamentsTeams>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<TournamentsTeams>>(teams, HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<TournamentsTeams> getTeam(@PathVariable("id") Integer id) {
        TournamentsTeams team = tournamentsTeamsService.singleTournamentTeamById(id);
        if (team == null) {
            return new ResponseEntity<TournamentsTeams>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<TournamentsTeams>(team, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createTeam(@RequestBody TournamentsTeams team, UriComponentsBuilder componentsBuilderl) {
        if (tournamentsTeamsService.isExist(team)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        tournamentsTeamsService.create(team);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(componentsBuilderl.path("/team/{id}").buildAndExpand(team.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    /*@RequestMapping( method = RequestMethod.PUT)
    public ResponseEntity<Team> updateTeam(@RequestBody  Team team) {
        Team currentTeam = teamService.findById(team.getId());
        if (currentTeam == null) {
            return new ResponseEntity<Team>(HttpStatus.NOT_FOUND);
        }
        currentTeam.setTournament(team.getTournament());
        currentTeam.setSumPointsInTournament(team.getSumPointsInTournament());
        currentTeam.setTeamName(team.getTeamName());
        teamService.update(currentTeam);
        return new ResponseEntity<Team>(team, HttpStatus.OK);
    }*/

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Team> deleteTournamentById(@PathVariable("id") Integer id) {
        Team team = teamService.findById(id);
        if (team == null) {
            return new ResponseEntity<Team>(HttpStatus.NOT_FOUND);
        }
        teamService.delete(team);
        return new ResponseEntity<Team>(HttpStatus.NO_CONTENT);
    }
}
