package com.bestbets.app.controller;

import com.bestbets.app.model.Sport;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.service.SportService;
import com.bestbets.app.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/tournament")
public class TournamentController {
    @Autowired
    TournamentService tournamentService;
    @Autowired
    SportService sportService;
    @RequestMapping(value = {"/bysport/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<Tournament>> listSports(@PathVariable("id") Integer id) {
        Sport sport = sportService.findById(id);
        List<Tournament> tournaments = tournamentService.findBySport(sport);
        if (tournaments.isEmpty()) {
            return new ResponseEntity<List<Tournament>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Tournament>>(tournaments, HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Tournament> getTournament(@PathVariable("id") Integer id) {
        Tournament tournament = tournamentService.findById(id);
        if (tournament == null) {
            return new ResponseEntity<Tournament>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Tournament>(tournament, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createTournament(@RequestBody Tournament tournament, UriComponentsBuilder componentsBuilderl) {
        if (tournamentService.isExist(tournament)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        tournamentService.create(tournament);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(componentsBuilderl.path("/tournament/{id}").buildAndExpand(tournament.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Tournament> updateTournament(@RequestBody Tournament tournament) {
        Tournament currentTournament = tournamentService.findById(tournament.getId());
        if (currentTournament == null) {
            return new ResponseEntity<Tournament>(HttpStatus.NOT_FOUND);
        }
        currentTournament.setId(tournament.getId());
        currentTournament.setSport(tournament.getSport());
        currentTournament.setDateTime(tournament.getDateTime());
        currentTournament.setTournamentName(tournament.getTournamentName());
        tournamentService.update(currentTournament);
        return new ResponseEntity<Tournament>(tournament, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Tournament> deleteTournamentById(@PathVariable("id") Integer id) {
        Tournament tournament = tournamentService.findById(id);
        if (tournament == null) {
            return new ResponseEntity<Tournament>(HttpStatus.NOT_FOUND);
        }
        tournamentService.delete(tournament);
        return new ResponseEntity<Tournament>(HttpStatus.NO_CONTENT);
    }
}
