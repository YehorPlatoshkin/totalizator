package com.bestbets.app.controller;

import com.bestbets.app.model.Transfer;
import com.bestbets.app.model.UserAccount;
import com.bestbets.app.service.TransferService;
import com.bestbets.app.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/transfer")
public class TransferController {
    @Autowired
    TransferService transferService;
    @Autowired
    UserAccountService userAccountService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Transfer>> getTransfersByAccount(@PathVariable("id") Integer id){
        UserAccount userAccount = userAccountService.findById(id);
        if (userAccount == null){
            return new  ResponseEntity<List<Transfer>>(HttpStatus.NOT_FOUND);
        }
        List<Transfer> transferList = transferService.listByUserAccount(userAccount);
        if (transferList.isEmpty()){
            return new ResponseEntity<List<Transfer>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Transfer>>(transferList,HttpStatus.OK);
    }

}
