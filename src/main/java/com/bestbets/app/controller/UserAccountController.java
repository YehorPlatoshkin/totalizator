package com.bestbets.app.controller;

import com.bestbets.app.model.UserAccount;
import com.bestbets.app.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/useraccount")
public class UserAccountController {
    @Autowired
    UserAccountService userAccountService;


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<UserAccount>> getAllAccounts(){
        List<UserAccount> userAccounts = userAccountService.findAll();
        if(userAccounts.isEmpty()){
            return new ResponseEntity<List<UserAccount>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<UserAccount>>(userAccounts,HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserAccount> getByUser(@PathVariable("id") Integer id){
        UserAccount account = userAccountService.findById(id);
        if (account == null){
            return new ResponseEntity<UserAccount>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UserAccount>(account, HttpStatus.OK);
    }

    @RequestMapping(value = "/put/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserAccount> putMoneyToUser(@PathVariable("id") Integer id, @RequestBody BigDecimal moneyAmount){
        UserAccount account = userAccountService.findById(id);
        if (account == null){
            return new ResponseEntity<UserAccount>(HttpStatus.NOT_FOUND);
        }
        if (moneyAmount.compareTo(new BigDecimal(9999.99))==1){
            return new ResponseEntity<UserAccount>(HttpStatus.CONFLICT);
        }
        userAccountService.putMoneyToUser(account, moneyAmount);
        return new ResponseEntity<UserAccount>(account,HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserAccount> getMoneyFromUser(@PathVariable("id") Integer id,
                                                        @RequestBody BigDecimal moneyAmount){
        UserAccount account = userAccountService.findById(id);
        if (account == null){
            return new ResponseEntity<UserAccount>(HttpStatus.NOT_FOUND);
        }
        if (moneyAmount.compareTo(account.getBalance())==1){
            return new ResponseEntity<UserAccount>(HttpStatus.CONFLICT);
        }
        userAccountService.getMoneyFromUser(account, moneyAmount);
        return new ResponseEntity<UserAccount>(account,HttpStatus.OK);
    }
}
