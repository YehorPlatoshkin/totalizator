package com.bestbets.app.controller;

import org.springframework.web.bind.annotation.RestController;

@RestController
/*@RequestMapping("/user")*/
public class UserController {
/*    @Autowired
    UserService userService;
    @Autowired
    UserProfileService userProfileService;
    @Autowired
    UserAccountService userAccountService;
    @Autowired
    BetService betService;

    @RequestMapping(value = {"/user/{id}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("id") Integer id) {
        User user = userService.findById(id);
        if (user == null){
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<User>(user,HttpStatus.OK);
    }
    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public  ResponseEntity<User> updateUser(@PathVariable("id") Integer id, @RequestBody User user){
        User currentUser = userService.findById(id);
        if (currentUser==null){
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        userService.update(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }*/

    /*
    Bet
    */
    /*@RequestMapping(value = {"/bets"}, method = RequestMethod.GET)
    public ResponseEntity<List<Bet>> listBets(@RequestBody User user) {
        List<Bet> bets = betService.listBetsByUser(user);
        if (bets.isEmpty()) {
            return new ResponseEntity<List<Bet>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Bet>>(bets, HttpStatus.OK);
    }
    @RequestMapping(value = {"/currentbets"}, method = RequestMethod.GET)
    public ResponseEntity<List<Bet>> listCurrentBets(@RequestBody User user) {
        List<Bet> bets = betService.currentBetsByUser(user);
        if (bets.isEmpty()) {
            return new ResponseEntity<List<Bet>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Bet>>(bets, HttpStatus.OK);
    }
    @RequestMapping(value = {"/historybets"}, method = RequestMethod.GET)
    public ResponseEntity<List<Bet>> listHistoryBets(@RequestBody User user) {
        List<Bet> bets = betService.historyBetsByUser(user);
        if (bets.isEmpty()) {
            return new ResponseEntity<List<Bet>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Bet>>(bets, HttpStatus.OK);
    }

    @RequestMapping(value = {"/bet/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Bet> getBet(@PathVariable("id") Integer id) {
        Bet bet = betService.findById(id);
        if (bet == null) {
            return new ResponseEntity<Bet>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Bet>(bet, HttpStatus.OK);
    }

    @RequestMapping(value = {"/bet"}, method = RequestMethod.POST)
    public ResponseEntity<Void> createBet(@RequestBody Bet bet, UriComponentsBuilder componentsBuilderl,
                                         @RequestBody SportEvent sportEvent,@RequestBody User user) {
        if (betService.isBetExist(sportEvent, user)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        betService.save(bet);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(componentsBuilderl.path("/sport/{id}").buildAndExpand(bet.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/bet/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Bet> updateBet(@PathVariable("id") Integer id, @RequestBody Bet bet) {
        Bet currentBet = betService.findById(id);
        if (betService == null) {
            return new ResponseEntity<Bet>(HttpStatus.NOT_FOUND);
        }
        betService.update(bet);
        return new ResponseEntity<Bet>(bet, HttpStatus.OK);
    }*/
}
