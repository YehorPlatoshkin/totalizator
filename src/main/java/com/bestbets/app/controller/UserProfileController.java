package com.bestbets.app.controller;

import com.bestbets.app.model.UserProfile;
import com.bestbets.app.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/userprofile")
public class UserProfileController {
    @Autowired
    UserProfileService userProfileService;

    @RequestMapping(value = {"/all"}, method = RequestMethod.GET)
    public ResponseEntity<List<UserProfile>> listAllProfiles(){
        List<UserProfile> userProfiles = userProfileService.listAll();
        if (userProfiles.isEmpty()) {
            return new ResponseEntity<List<UserProfile>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<UserProfile>>(userProfiles, HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<UserProfile> getUserProfile(@PathVariable("id") Integer id){
        UserProfile userProfile = userProfileService.findById(id);
        if (userProfile == null) {
            return new ResponseEntity<UserProfile>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<UserProfile>(userProfile, HttpStatus.OK);
    }
}
