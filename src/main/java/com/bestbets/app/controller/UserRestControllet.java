package com.bestbets.app.controller;


import com.bestbets.app.model.User;
import com.bestbets.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
@RestController
@RequestMapping("/user")
public class UserRestControllet {
    @Autowired
    UserService userService;

    @RequestMapping(value = {"/workingstuff"}, method = RequestMethod.GET)
    public ResponseEntity<List<User>> listWorkingStuff() {
        List<User> workingStuff = userService.listWorkingStuff();
        if (workingStuff.isEmpty()){
            return  new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(workingStuff,HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("id") Integer id) {
        User user = userService.findById(id);
        if (user == null){
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<User>(user,HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody User user, UriComponentsBuilder componentsBuilderl) {
        if (userService.isUserExist(user)) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        userService.create(user);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(componentsBuilderl.path("/user/{id}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    @RequestMapping(method = RequestMethod.PUT)
    public  ResponseEntity<User> updateUser(@RequestBody User user){
        User currentUser = userService.findById(user.getId());
        if (currentUser==null){
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        currentUser.setFullName(user.getFullName());
        currentUser.setEmail(user.getEmail());
        currentUser.setPassword(user.getPassword());
        currentUser.setState(user.getState());
        currentUser.setProfile(user.getProfile());
        currentUser.setBetList(user.getBetList());
        currentUser.setAccount(user.getAccount());
        userService.update(currentUser);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") Integer id) {
        User user = userService.findById(id);
        if (user == null){
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        userService.deleteById(id);
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }
}
