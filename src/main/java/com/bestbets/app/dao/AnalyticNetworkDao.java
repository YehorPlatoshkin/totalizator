package com.bestbets.app.dao;


import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.AnalyticNetwork;
import com.bestbets.app.model.Sport;

public interface AnalyticNetworkDao extends GenericDao<Integer,AnalyticNetwork> {
    AnalyticNetwork findBySport(Sport sport);
}
