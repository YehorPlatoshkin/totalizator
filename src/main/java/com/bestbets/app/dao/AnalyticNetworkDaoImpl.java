package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.AnalyticNetwork;
import com.bestbets.app.model.Sport;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("analyticNetworkDao")
public class AnalyticNetworkDaoImpl extends AbstractDao<Integer, AnalyticNetwork> implements AnalyticNetworkDao {
    public AnalyticNetwork findBySport(Sport sport) {
        Criteria criteria = createEntityCriteria().add(Restrictions.eq("sport", sport));
        return (AnalyticNetwork) criteria.uniqueResult();
    }
}
