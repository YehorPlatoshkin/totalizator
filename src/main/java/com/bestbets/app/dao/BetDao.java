package com.bestbets.app.dao;


import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.Bet;
import com.bestbets.app.model.BetMark;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.User;

import java.util.List;

public interface BetDao extends GenericDao<Integer, Bet> {
    Bet findBySportEventAndUser(SportEvent sportEvent, User user);

    /*USERS*/
    List<Bet> findByUserAndState(User user, String state);

    List<Bet> findBetsByUser(User user);

    /*SPORTS EVENTS*/
    List<Bet> findBySportEventAndBetMark(SportEvent sportEvent, BetMark betMark);

    List<Bet> findBySportEvent(SportEvent sportEvent);
}
