package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.Bet;
import com.bestbets.app.model.BetMark;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("betDao")
public class BetDaoImpl extends AbstractDao<Integer, Bet> implements BetDao {

    @Override
    public Bet findBySportEventAndUser(SportEvent sportEvent, User user) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("sportEvent", sportEvent));
        criteria.add(Restrictions.eq("user", user));
        return (Bet) criteria.uniqueResult();
    }

    /*USERS*/

    @SuppressWarnings("unchecked")
    @Override
    public List<Bet> findByUserAndState(User user, String state) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("user", user));
        criteria.add(Restrictions.eq("state", state));
        return (List<Bet>)criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Bet> findBetsByUser(User user) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("user", user));
        return (List<Bet>) criteria.list();
    }

    /*SPORT EVENTS*/

    @SuppressWarnings("unchecked")
    @Override
    public List<Bet> findBySportEventAndBetMark(SportEvent sportEvent, BetMark betMark) {
        Criteria criteria = createEntityCriteria();
        Criterion findSportEvent = Restrictions.eq("sportEvent", sportEvent);
        Criterion findMark = Restrictions.eq("betMark", betMark);
        LogicalExpression andExp = Restrictions.and(findSportEvent, findMark);
        criteria.add(andExp);
        return (List<Bet>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Bet> findBySportEvent(SportEvent sportEvent) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("sportEvent", sportEvent));
        return (List<Bet>) criteria.list();
    }
}
