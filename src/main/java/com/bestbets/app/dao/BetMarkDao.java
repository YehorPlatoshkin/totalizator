package com.bestbets.app.dao;

import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.BetMark;

public interface BetMarkDao extends GenericDao<Integer, BetMark> {
    BetMark findByType(String type);
}
