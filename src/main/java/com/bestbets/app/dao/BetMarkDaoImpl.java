package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.BetMark;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("betMarkDao")
public class BetMarkDaoImpl extends AbstractDao<Integer, BetMark> implements BetMarkDao {


    public BetMark findByType(String type) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("type", type));
        return (BetMark) criteria.uniqueResult();
    }

}
