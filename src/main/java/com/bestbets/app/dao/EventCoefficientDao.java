package com.bestbets.app.dao;


import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.BetMark;
import com.bestbets.app.model.EventCoefficient;
import com.bestbets.app.model.SportEvent;

import java.util.List;

public interface EventCoefficientDao extends GenericDao<Integer, EventCoefficient> {
    EventCoefficient findBySportEventAndBetMark(SportEvent sportEvent, BetMark betMark);

    List<EventCoefficient> findBySportEvent(SportEvent sportEvent);

}
