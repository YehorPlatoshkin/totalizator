package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.BetMark;
import com.bestbets.app.model.EventCoefficient;
import com.bestbets.app.model.SportEvent;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("eventCoefficientDao")
public class EventCoefficientDaoImpl extends AbstractDao<Integer, EventCoefficient> implements EventCoefficientDao {


    @Override
    public EventCoefficient findBySportEventAndBetMark(SportEvent sportEvent, BetMark betMark) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("sportEvent", sportEvent));
        criteria.add(Restrictions.eq("betMark",betMark));
        return (EventCoefficient) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EventCoefficient> findBySportEvent(SportEvent sportEvent) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("sportEvent", sportEvent));
        return (List<EventCoefficient>) criteria.list();
    }
}
