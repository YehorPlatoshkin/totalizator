package com.bestbets.app.dao;


import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.ResultSet;
import com.bestbets.app.model.SportEvent;

public interface ResultSetDao extends GenericDao<Integer, ResultSet> {
    ResultSet findBySpotEvent(SportEvent sportEvent);
}
