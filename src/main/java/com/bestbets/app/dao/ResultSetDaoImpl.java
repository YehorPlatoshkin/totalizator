package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.ResultSet;
import com.bestbets.app.model.SportEvent;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("resultSetDao")
public class ResultSetDaoImpl extends AbstractDao<Integer, ResultSet> implements ResultSetDao {
    @Override
    public ResultSet findBySpotEvent(SportEvent sportEvent) {
        return (ResultSet) createEntityCriteria()
                .add(Restrictions.eq("sportEvent", sportEvent))
                .uniqueResult();
    }
}
