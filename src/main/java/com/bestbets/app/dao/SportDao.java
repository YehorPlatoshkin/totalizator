package com.bestbets.app.dao;

import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.Sport;

public interface SportDao extends GenericDao<Integer, Sport> {
    Sport findByName(String name);
}
