package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.Sport;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("sportDao")
public class SportDaoImpl extends AbstractDao<Integer, Sport> implements SportDao {

    @Override
    public Sport findByName(String name) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("sportName", name));
        return (Sport) criteria.uniqueResult();
    }

}
