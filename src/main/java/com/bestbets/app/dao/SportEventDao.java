package com.bestbets.app.dao;


import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.TournamentsTeams;
import org.joda.time.LocalDateTime;

import java.util.List;

public interface SportEventDao extends GenericDao<Integer, SportEvent> {
    SportEvent findByMeeting(TournamentsTeams homeTeam, TournamentsTeams guestTeam, LocalDateTime dateTime);

    @SuppressWarnings("unchecked")
    List<SportEvent> findByTeam(TournamentsTeams team);

    /*Current*/
    List<SportEvent> findCurrentEvents();

    List<SportEvent> findCurrentEventsByTeam(TournamentsTeams team);

    /*Past*/
    List<SportEvent> findPastEvents();

    List<SportEvent> findPastEventsByTeam(TournamentsTeams team, LocalDateTime dateTime);

    @SuppressWarnings("unchecked")
    List<SportEvent> findPastEventsByHomeTeam(TournamentsTeams team, LocalDateTime dateTime);

    @SuppressWarnings("unchecked")
    List<SportEvent> findPastEventsByGuestTeam(TournamentsTeams team, LocalDateTime dateTime);

    /*Temporary*/
    List<SportEvent> findAllOrderByDate();


}
