package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.TournamentsTeams;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("sportEventDao")
public class SportEventDaoImpl extends AbstractDao<Integer, SportEvent> implements SportEventDao {

    @Override
    public SportEvent findByMeeting(TournamentsTeams homeTeam, TournamentsTeams guestTeam, LocalDateTime dateTime) {
        Criteria criteria = createEntityCriteria();
        Criterion findHomeTeamName = Restrictions.eq("homeTeam", homeTeam);
        Criterion findGuestTeamName = Restrictions.eq("guestTeam", guestTeam);
        Criterion findDateTime = Restrictions.eq("eventBeginningDate", dateTime);
        LogicalExpression andExp = Restrictions.and(Restrictions.and(findHomeTeamName,findGuestTeamName),findDateTime);
        criteria.add(andExp);
        return (SportEvent) criteria.uniqueResult();
    }
    @SuppressWarnings("unchecked")
    @Override
    public List<SportEvent> findByTeam(TournamentsTeams team){
        return  (List<SportEvent>)createEntityCriteria()
                .add(Restrictions
                        .or(Restrictions.eq("homeTeam", team), Restrictions.eq("guestTeam", team)))
                .list();
    }
    /*CURRENT EVENTS*/

    @SuppressWarnings("unchecked")
    @Override
    public List<SportEvent> findCurrentEvents() {
        String querySQL = "SELECT sevent.* FROM sport_event sevent \n" +
                "LEFT JOIN result_set rset \n" +
                "ON sevent.SPORT_EVENT_ID = rset.SPORT_EVENT_ID WHERE rset.SPORT_EVENT_ID IS NULL " +
                "ORDER BY sevent.EVENT_BEGINNING_DATE ASC; ";
        SQLQuery sqlQuery = getSession()
                .createSQLQuery(querySQL);
        sqlQuery.addEntity(SportEvent.class);
        return (List<SportEvent>) sqlQuery
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SportEvent> findCurrentEventsByTeam(TournamentsTeams team) {
        String querySQL = "SELECT sevent.* FROM sport_event sevent \n" +
                "LEFT JOIN result_set rset \n" +
                "ON sevent.SPORT_EVENT_ID = rset.SPORT_EVENT_ID WHERE rset.SPORT_EVENT_ID IS NULL " +
                "AND (sevent.HOME_TEAM_ID = :id OR sevent.GUEST_TEAM_ID = :id)";
        SQLQuery sqlQuery = getSession()
                .createSQLQuery(querySQL);
        sqlQuery.setParameter("id", team.getId());
        sqlQuery.addEntity(SportEvent.class);
        return (List<SportEvent>) sqlQuery
                .list();
    }


    /*PAST EVENTS*/

    @SuppressWarnings("unchecked")
    @Override
    public List<SportEvent> findPastEvents() {
        String querySQL = "SELECT sevent.* FROM sport_event sevent \n" +
                "LEFT JOIN `result_set` `rset`" +
                "ON `sevent`.`SPORT_EVENT_ID` = `rset`.`SPORT_EVENT_ID` " +
                "WHERE `rset`.`SPORT_EVENT_ID` IS NOT NULL " +
                "ORDER BY sevent.EVENT_BEGINNING_DATE DESC; ";
        SQLQuery sqlQuery = getSession()
                .createSQLQuery(querySQL);
        sqlQuery.addEntity(SportEvent.class);
        return (List<SportEvent>) sqlQuery
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SportEvent> findPastEventsByTeam(TournamentsTeams team, LocalDateTime dateTime ) {
        String querySQL = "SELECT sevent.* FROM sport_event sevent \n" +
                "LEFT JOIN `result_set` `rset`" +
                "ON `sevent`.`SPORT_EVENT_ID` = `rset`.`SPORT_EVENT_ID` " +
                "WHERE `rset`.`SPORT_EVENT_ID` IS NOT NULL " +
                "and `sevent`.`EVENT_BEGINNING_DATE` < :date "+
                "AND (sevent.HOME_TEAM_ID = :id OR sevent.GUEST_TEAM_ID = :id)" +
                "ORDER BY sevent.EVENT_BEGINNING_DATE DESC;";
        SQLQuery sqlQuery = getSession()
                .createSQLQuery(querySQL);
        sqlQuery.setParameter("id", team.getId());
        sqlQuery.setParameter("date", dateTime.toString("yyyy-MM-dd hh:mm:ss"));
        sqlQuery.addEntity(SportEvent.class);
        return (List<SportEvent>) sqlQuery
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SportEvent> findPastEventsByHomeTeam(TournamentsTeams team, LocalDateTime dateTime) {
        String querySQL = "SELECT sevent.* FROM sport_event sevent \n" +
                "LEFT JOIN `result_set` `rset`" +
                "ON `sevent`.`SPORT_EVENT_ID` = `rset`.`SPORT_EVENT_ID` " +
                "WHERE `rset`.`SPORT_EVENT_ID` IS NOT NULL " +
                "and `sevent`.`EVENT_BEGINNING_DATE` < :date "+
                "AND (sevent.GUEST_TEAM_ID = :id)" +
                "ORDER BY sevent.EVENT_BEGINNING_DATE DESC;";
        SQLQuery sqlQuery = getSession()
                .createSQLQuery(querySQL);
        sqlQuery.setParameter("id", team.getId());
        sqlQuery.setParameter("date", dateTime.toString("yyyy-MM-dd hh:mm:ss"));
        sqlQuery.addEntity(SportEvent.class);
        return (List<SportEvent>) sqlQuery
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SportEvent> findPastEventsByGuestTeam(TournamentsTeams team, LocalDateTime dateTime) {
        String querySQL = "SELECT sevent.* FROM sport_event sevent \n" +
                "LEFT JOIN `result_set` `rset`" +
                "ON `sevent`.`SPORT_EVENT_ID` = `rset`.`SPORT_EVENT_ID` " +
                "WHERE `rset`.`SPORT_EVENT_ID` IS NOT NULL " +
                "and `sevent`.`EVENT_BEGINNING_DATE` < :date "+
                "AND (sevent.HOME_TEAM_ID = :id )" +
                "ORDER BY sevent.EVENT_BEGINNING_DATE DESC;";
        SQLQuery sqlQuery = getSession()
                .createSQLQuery(querySQL);
        sqlQuery.setParameter("id", team.getId());
        sqlQuery.setParameter("date", dateTime.toString("yyyy-MM-dd hh:mm:ss"));
        sqlQuery.addEntity(SportEvent.class);
        return (List<SportEvent>) sqlQuery
                .list();
    }

    /*TEMPORARY*/

    @SuppressWarnings("unchecked")
    @Override
    public List<SportEvent> findAllOrderByDate() {
        return (List<SportEvent>)createEntityCriteria()
                .addOrder(Order.desc("dateTime"))
                .list();
    }

}
