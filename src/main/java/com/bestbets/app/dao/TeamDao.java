package com.bestbets.app.dao;


import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.Team;

public interface TeamDao extends GenericDao<Integer, Team> {
    Team findByName(String teamName);
}
