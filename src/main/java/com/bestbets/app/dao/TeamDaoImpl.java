package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.Team;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("teamDao")
public class TeamDaoImpl extends AbstractDao<Integer, Team> implements TeamDao {

    @Override
    public Team findByName(String teamName) {
        return (Team) createEntityCriteria().add(Restrictions.eq("teamName",teamName)).uniqueResult();
    }
}
