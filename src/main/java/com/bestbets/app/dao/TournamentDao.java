package com.bestbets.app.dao;

import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.Sport;
import com.bestbets.app.model.Tournament;
import org.joda.time.LocalDateTime;

import java.util.List;

public interface TournamentDao extends GenericDao<Integer, Tournament> {
    Tournament findByNameAndYear(String name, LocalDateTime date);

    List<Tournament> findBySport(Sport sport);
}
