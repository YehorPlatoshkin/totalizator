package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.Sport;
import com.bestbets.app.model.Tournament;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("tournamentDao")
public class TournamentDaoImpl extends AbstractDao<Integer, Tournament> implements TournamentDao {

    @Override
    public Tournament findByNameAndYear(String name, LocalDateTime date) {
        Criteria criteria = createEntityCriteria();
        Criterion findName = Restrictions.eq("tournamentName", name);
        Criterion findDate = Restrictions.eq("tournamentBeginningDate", date);
        LogicalExpression andExp = Restrictions.and(findName, findDate);
        criteria.add(andExp);
        return (Tournament) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Tournament> findBySport(Sport sport) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("sport", sport));
        return (List<Tournament>) criteria.list();
    }
}
