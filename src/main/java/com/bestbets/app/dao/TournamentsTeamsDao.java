package com.bestbets.app.dao;


import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.Team;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.model.TournamentsTeams;

import java.util.List;

public interface TournamentsTeamsDao extends GenericDao<Integer, TournamentsTeams> {
    TournamentsTeams findSingleTournamenTeam(Tournament tournament, Team team);

    List<TournamentsTeams> findByTournament(Tournament tournament);

    List<TournamentsTeams> findByTeam(Team team);
}
