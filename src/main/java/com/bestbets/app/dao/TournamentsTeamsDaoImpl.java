package com.bestbets.app.dao;


import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.Team;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.model.TournamentsTeams;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("tournamentsTeamsDao")
public class TournamentsTeamsDaoImpl extends AbstractDao<Integer, TournamentsTeams> implements TournamentsTeamsDao {

    @Override
    public TournamentsTeams findSingleTournamenTeam(Tournament tournament, Team team) {
        return (TournamentsTeams) createEntityCriteria()
                .add(Restrictions.eq("tournament", tournament))
                .add(Restrictions.eq("team", team)).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TournamentsTeams> findByTournament(Tournament tournament) {
        return (List<TournamentsTeams>) createEntityCriteria()
                .add(Restrictions.eq("tournament", tournament))
                .addOrder(Order.desc("sumPointsInTournament")).list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TournamentsTeams> findByTeam(Team team) {
        return (List<TournamentsTeams>) createEntityCriteria()
                .add(Restrictions.eq("team", team)).list();
    }
}
