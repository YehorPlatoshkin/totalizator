package com.bestbets.app.dao;


import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.Transfer;
import com.bestbets.app.model.UserAccount;

import java.util.List;

public interface TransferDao extends GenericDao<Integer,Transfer> {
    List<Transfer> findByUserAccount(UserAccount userAccount);
}
