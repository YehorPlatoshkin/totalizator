package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.Transfer;
import com.bestbets.app.model.UserAccount;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("transferDao")
public class TransferDaoImpl extends AbstractDao<Integer, Transfer> implements TransferDao {

    @SuppressWarnings("unchecked")
    @Override
    public List<Transfer> findByUserAccount(UserAccount userAccount) {
        return (List<Transfer>) createEntityCriteria().add(Restrictions.eq("userAccount",userAccount)).list();
    }
}
