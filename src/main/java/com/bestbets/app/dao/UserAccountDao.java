package com.bestbets.app.dao;

import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.User;
import com.bestbets.app.model.UserAccount;

public interface UserAccountDao extends GenericDao<Integer, UserAccount> {
    UserAccount findByUser(User user);
}
