package com.bestbets.app.dao;


import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.User;
import com.bestbets.app.model.UserAccount;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("userAccountDao")
public class UserAccountDaoImpl extends AbstractDao<Integer, UserAccount> implements UserAccountDao {

    @Override
    public UserAccount findByUser(User user) {
        return (UserAccount) createEntityCriteria()
                .add(Restrictions.eq("user", user))
                .uniqueResult();
    }
}
