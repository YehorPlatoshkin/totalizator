package com.bestbets.app.dao;


import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.User;
import com.bestbets.app.model.UserProfile;

import java.util.List;

public interface UserDao extends GenericDao<Integer,User> {
    User findByEmail(String email);
    List<User> findByProfile(UserProfile userProfile);
}
