package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.User;
import com.bestbets.app.model.UserProfile;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {


    @Override
    public User findByEmail(String email) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("email", email));
        return (User) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> findByProfile(UserProfile userProfile) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("profile", userProfile));
        return (List<User>) criteria.list();
    }

}
