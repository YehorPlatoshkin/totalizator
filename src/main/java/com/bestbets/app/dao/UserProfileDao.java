package com.bestbets.app.dao;

import com.bestbets.app.dao.support.GenericDao;
import com.bestbets.app.model.UserProfile;

public interface UserProfileDao extends GenericDao<Integer, UserProfile> {
    UserProfile findByType(String type);
}
