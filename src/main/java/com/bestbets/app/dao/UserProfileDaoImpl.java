package com.bestbets.app.dao;

import com.bestbets.app.dao.support.AbstractDao;
import com.bestbets.app.model.UserProfile;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("userProfileDao")
public class UserProfileDaoImpl extends AbstractDao<Integer, UserProfile> implements UserProfileDao {


    public UserProfile findByType(String type) {
        return (UserProfile) createEntityCriteria()
                .add(Restrictions.eq("type", type))
                .uniqueResult();
    }
}
