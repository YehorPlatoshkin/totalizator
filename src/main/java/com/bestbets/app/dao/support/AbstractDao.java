package com.bestbets.app.dao.support;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractDao<PK extends Serializable, T> implements GenericDao<PK ,T> {
    @Autowired
    private SessionFactory sessionFactory;
    private final Class<T> persistentClass;

    @SuppressWarnings("unchecked")
        public AbstractDao() {
            this.persistentClass = (Class<T>)((ParameterizedType) this.getClass()
                    .getGenericSuperclass())
                    .getActualTypeArguments()[1];
        }



    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    @Override
    public T findById(PK id) {
        return (T)getSession().get(persistentClass,id);
    }

    @Override
    public void save(T entity) {
        getSession().persist(entity);
    }

    @Override
    public void update(T entity) {
        getSession().update(entity);
    }

    @Override
    public void delete(T entity) {
        getSession().delete(entity);
    }

    public Criteria createEntityCriteria(){
        return getSession().createCriteria(persistentClass);
    }
    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAll() {
        return (List<T>) createEntityCriteria().list();
    }
}
