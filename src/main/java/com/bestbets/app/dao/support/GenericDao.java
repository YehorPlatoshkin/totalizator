package com.bestbets.app.dao.support;


import java.io.Serializable;
import java.util.List;

public interface GenericDao<PK extends Serializable, T> {
    T findById(PK id);
    void save(T entity);
    void update(T entity);
    void delete(T entity);
    List<T> findAll();

}
