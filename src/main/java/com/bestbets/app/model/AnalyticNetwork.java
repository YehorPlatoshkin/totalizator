package com.bestbets.app.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "ANALYTIC_NETWORK")
public class AnalyticNetwork {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ANALYTIC_NETWORK_ID")
    private Integer id;

    @OneToOne()
    @JoinColumn(name = "SPORT_ID"/*, nullable = false*/)
    /*@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)*/
    @JsonBackReference
    private Sport sport;

    @Column(name = "NEURAL_NETWORK")
    private String neuralNetworkPath;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public String getNeuralNetworkPath() {
        return neuralNetworkPath;
    }

    public void setNeuralNetworkPath(String neuralNetworkPath) {
        this.neuralNetworkPath = neuralNetworkPath;
    }
}
