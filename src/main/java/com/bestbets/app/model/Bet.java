package com.bestbets.app.model;

import com.bestbets.app.model.support.BetState;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "BET")
@AttributeOverride( name="dateTime", column = @Column(name="BET_CREATING_DATE") )
public class Bet extends SuperDateTime{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BET_ID")
    private Integer id;

    @ManyToOne()
    @JoinColumn(name = "BET_MARK_ID")
    private BetMark betMark;

    @ManyToOne()
    @JoinColumn(name = "USER_ID")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private User user;

    @ManyToOne()
    @JoinColumn(name = "SPORT_EVENT_ID")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private SportEvent sportEvent;

    @Column(name = "BET_AMOUNT", precision = 8, scale = 2)
    private BigDecimal betAmount;

    @Column(name = "BET_STATE")
    private String state = BetState.PENDING.getBetState();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BetMark getBetMark() {
        return betMark;
    }

    public void setBetMark(BetMark betMark) {
        this.betMark = betMark;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SportEvent getSportEvent() {
        return sportEvent;
    }

    public void setSportEvent(SportEvent sportEvent) {
        this.sportEvent = sportEvent;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Bet bet = (Bet) o;

        if (id != null ? !id.equals(bet.id) : bet.id != null) return false;
        if (betMark != null ? !betMark.equals(bet.betMark) : bet.betMark != null) return false;
        if (user != null ? !user.equals(bet.user) : bet.user != null) return false;
        if (sportEvent != null ? !sportEvent.equals(bet.sportEvent) : bet.sportEvent != null) return false;
        if (betAmount != null ? !betAmount.equals(bet.betAmount) : bet.betAmount != null) return false;
        return state != null ? state.equals(bet.state) : bet.state == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (betMark != null ? betMark.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (sportEvent != null ? sportEvent.hashCode() : 0);
        result = 31 * result + (betAmount != null ? betAmount.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }
}
