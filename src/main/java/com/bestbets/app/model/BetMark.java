package com.bestbets.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "BET_MARK")
public class BetMark {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BET_MARK_ID")
    private Integer id;
    @Column(name = "TYPE", length = 15, unique = true, nullable = false)
    private String type;
    @OneToMany(mappedBy = "betMark", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Bet> bets;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BetMark betMark = (BetMark) o;

        if (id != null ? !id.equals(betMark.id) : betMark.id != null) return false;
        if (type != null ? !type.equals(betMark.type) : betMark.type != null) return false;
        return bets != null ? bets.equals(betMark.bets) : betMark.bets == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (bets != null ? bets.hashCode() : 0);
        return result;
    }
}
