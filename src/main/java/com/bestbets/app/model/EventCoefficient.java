package com.bestbets.app.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "EVENT_COEFFICIENT")
public class EventCoefficient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EVENT_COEFFICIENT_ID")
    private Integer id;
    @ManyToOne()
    @JoinColumn(name = "BET_MARK_ID")
    private BetMark betMark;
    @ManyToOne()
    @JoinColumn(name = "SPORT_EVENT_ID")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private SportEvent sportEvent;
    @Column(name = "COEFFICIENT", precision = 8, scale = 2)
    private BigDecimal coefficient;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BetMark getBetMark() {
        return betMark;
    }

    public void setBetMark(BetMark betMark) {
        this.betMark = betMark;
    }

    public SportEvent getSportEvent() {
        return sportEvent;
    }

    public void setSportEvent(SportEvent sportEvent) {
        this.sportEvent = sportEvent;
    }

    public BigDecimal getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(BigDecimal coefficient) {
        this.coefficient = coefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventCoefficient that = (EventCoefficient) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (betMark != null ? !betMark.equals(that.betMark) : that.betMark != null) return false;
        if (sportEvent != null ? !sportEvent.equals(that.sportEvent) : that.sportEvent != null) return false;
        return coefficient != null ? coefficient.equals(that.coefficient) : that.coefficient == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (betMark != null ? betMark.hashCode() : 0);
        result = 31 * result + (sportEvent != null ? sportEvent.hashCode() : 0);
        result = 31 * result + (coefficient != null ? coefficient.hashCode() : 0);
        return result;
    }
}
