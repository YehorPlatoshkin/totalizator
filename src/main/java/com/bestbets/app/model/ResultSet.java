package com.bestbets.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "RESULT_SET")
public class ResultSet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RESULT_SET_ID")
    private Integer id;
    @Column(name = "HOME_TEAM_POINTS")
    private Integer homeTeamPoints;
    @Column(name = "GUEST_TEAM_POINTS")
    private Integer guestTeamPoints;
    @OneToOne()
    @JoinColumn(name = "SPORT_EVENT_ID")
    /*@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)*/
    @JsonIgnore
    private SportEvent sportEvent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHomeTeamPoints() {
        return homeTeamPoints;
    }

    public void setHomeTeamPoints(Integer homeTeamPoints) {
        this.homeTeamPoints = homeTeamPoints;
    }

    public Integer getGuestTeamPoints() {
        return guestTeamPoints;
    }

    public void setGuestTeamPoints(Integer guestTeamPoints) {
        this.guestTeamPoints = guestTeamPoints;
    }

    public SportEvent getSportEvent() {
        return sportEvent;
    }

    public void setSportEvent(SportEvent sportEvent) {
        this.sportEvent = sportEvent;
    }
}
