package com.bestbets.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "SPORT")
public class Sport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SPORT_ID")
    private Integer id;

    @Column(name = "SPORT_NAME", nullable = false, unique = true)
    private String sportName;
    @JsonIgnore
    @OneToMany(mappedBy = "sport", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Tournament> tournaments;

    @OneToOne(mappedBy = "sport", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    /*@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)*/
    @JsonManagedReference
    private AnalyticNetwork analyticNetwork;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSportName() {
        return sportName;
    }

    public void setSportName(String sportName) {
        this.sportName = sportName;
    }

    public List<Tournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<Tournament> tournaments) {
        this.tournaments = tournaments;
    }

    public AnalyticNetwork getAnalyticNetwork() {
        return analyticNetwork;
    }

    public void setAnalyticNetwork(AnalyticNetwork analyticNetwork) {
        this.analyticNetwork = analyticNetwork;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sport sport = (Sport) o;

        if (id != null ? !id.equals(sport.id) : sport.id != null) return false;
        if (sportName != null ? !sportName.equals(sport.sportName) : sport.sportName != null) return false;
        if (tournaments != null ? !tournaments.equals(sport.tournaments) : sport.tournaments != null) return false;
        return analyticNetwork != null ? analyticNetwork.equals(sport.analyticNetwork) : sport.analyticNetwork == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (sportName != null ? sportName.hashCode() : 0);
        result = 31 * result + (tournaments != null ? tournaments.hashCode() : 0);
        result = 31 * result + (analyticNetwork != null ? analyticNetwork.hashCode() : 0);
        return result;
    }
}
