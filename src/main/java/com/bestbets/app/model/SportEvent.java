package com.bestbets.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "SPORT_EVENT")
@AttributeOverride( name="dateTime", column = @Column(name="EVENT_BEGINNING_DATE") )
public class SportEvent extends SuperDateTime {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SPORT_EVENT_ID")
    private Integer id;

    @ManyToOne()
    @JoinColumn(name = "HOME_TEAM_ID"/*, referencedColumnName = "TOURNAMENT_TEAM_ID", insertable = false, updatable = false*/)
    private TournamentsTeams homeTeam;

    @ManyToOne()
    @JoinColumn(name = "GUEST_TEAM_ID"/*, referencedColumnName = "TOURNAMENT_TEAM_ID"*//*, insertable = false, updatable = false*/)
    private TournamentsTeams guestTeam;

    @OneToOne(mappedBy = "sportEvent")
    private ResultSet resultSet;

    @OneToMany(mappedBy = "sportEvent")
    private List<EventCoefficient> coefficients;

    @JsonIgnore
    @OneToMany(mappedBy = "sportEvent", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Bet> betList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TournamentsTeams getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(TournamentsTeams homeTeam) {
        this.homeTeam = homeTeam;
    }

    public TournamentsTeams getGuestTeam() {
        return guestTeam;
    }

    public void setGuestTeam(TournamentsTeams guestTeam) {
        this.guestTeam = guestTeam;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public List<EventCoefficient> getCoefficients() {
        return coefficients;
    }

    public void setCoefficients(List<EventCoefficient> coefficients) {
        this.coefficients = coefficients;
    }

    public List<Bet> getBetList() {
        return betList;
    }

    public void setBetList(List<Bet> betList) {
        this.betList = betList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SportEvent that = (SportEvent) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (homeTeam != null ? !homeTeam.equals(that.homeTeam) : that.homeTeam != null) return false;
        if (guestTeam != null ? !guestTeam.equals(that.guestTeam) : that.guestTeam != null) return false;
        if (resultSet != null ? !resultSet.equals(that.resultSet) : that.resultSet != null) return false;
        if (coefficients != null ? !coefficients.equals(that.coefficients) : that.coefficients != null) return false;
        return betList != null ? betList.equals(that.betList) : that.betList == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (homeTeam != null ? homeTeam.hashCode() : 0);
        result = 31 * result + (guestTeam != null ? guestTeam.hashCode() : 0);
        result = 31 * result + (resultSet != null ? resultSet.hashCode() : 0);
        result = 31 * result + (coefficients != null ? coefficients.hashCode() : 0);
        result = 31 * result + (betList != null ? betList.hashCode() : 0);
        return result;
    }
}
