package com.bestbets.app.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "TOURNAMENT")
@AttributeOverride( name="dateTime", column = @Column(name="TOURNAMENT_BEGINNING_DATE") )
public class Tournament extends SuperDateTime  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOURNAMENT_ID")
    private Integer id;

    @Column(name = "TOURNAMENT_NAME")
    private String tournamentName;

    @ManyToOne()
    @JoinColumn(name = "SPORT_ID")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Sport sport;
    @JsonIgnore
    @OneToMany(mappedBy = "tournament", fetch = FetchType.LAZY)
    private List<TournamentsTeams> teams;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public List<TournamentsTeams> getTeams() {
        return teams;
    }

    public void setTeams(List<TournamentsTeams> teams) {
        this.teams = teams;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Tournament that = (Tournament) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (tournamentName != null ? !tournamentName.equals(that.tournamentName) : that.tournamentName != null)
            return false;
        if (sport != null ? !sport.equals(that.sport) : that.sport != null) return false;
        return teams != null ? teams.equals(that.teams) : that.teams == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (tournamentName != null ? tournamentName.hashCode() : 0);
        result = 31 * result + (sport != null ? sport.hashCode() : 0);
        result = 31 * result + (teams != null ? teams.hashCode() : 0);
        return result;
    }
}
