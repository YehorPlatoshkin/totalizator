package com.bestbets.app.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table(name = "TOURNAMENT_TEAM")
public class TournamentsTeams {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOURNAMENT_TEAM_ID")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TOURNAMENT_ID")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Tournament tournament;

    @ManyToOne()
    @JoinColumn(name = "TEAM_ID")
    private Team team;

    @Column(name = "SUM_POINTS")
    private Integer sumPointsInTournament = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Integer getSumPointsInTournament() {
        return sumPointsInTournament;
    }

    public void setSumPointsInTournament(Integer sumPointsInTournament) {
        this.sumPointsInTournament = sumPointsInTournament;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TournamentsTeams that = (TournamentsTeams) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (tournament != null ? !tournament.equals(that.tournament) : that.tournament != null) return false;
        if (team != null ? !team.equals(that.team) : that.team != null) return false;
        return sumPointsInTournament != null ? sumPointsInTournament.equals(that.sumPointsInTournament) : that.sumPointsInTournament == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (tournament != null ? tournament.hashCode() : 0);
        result = 31 * result + (team != null ? team.hashCode() : 0);
        result = 31 * result + (sumPointsInTournament != null ? sumPointsInTournament.hashCode() : 0);
        return result;
    }
}
