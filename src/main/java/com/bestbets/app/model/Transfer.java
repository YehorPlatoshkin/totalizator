package com.bestbets.app.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "TRANS")
@AttributeOverride( name="dateTime", column = @Column(name="TRANSFER_DATE") )
public class Transfer extends SuperDateTime{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TRANSFER_ID")
    private Integer id;

    @Column(name = "BEFORE", precision = 8, scale = 2)
    private BigDecimal before;

    @Column(name = "MONEY_AMOUNT", precision = 8, scale = 2)
    private BigDecimal moneyAmount;

    @Column(name = "AFTER", precision = 8, scale = 2)
    private BigDecimal after;

    @Column(name = "ACTION")
    private String action;

    @ManyToOne()
    @JoinColumn(name = "USER_ACCOUNT_ID")
    private UserAccount userAccount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getBefore() {
        return before;
    }

    public void setBefore(BigDecimal before) {
        this.before = before;
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(BigDecimal moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public BigDecimal getAfter() {
        return after;
    }

    public void setAfter(BigDecimal after) {
        this.after = after;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Transfer transfer = (Transfer) o;

        if (id != null ? !id.equals(transfer.id) : transfer.id != null) return false;
        if (before != null ? !before.equals(transfer.before) : transfer.before != null) return false;
        if (moneyAmount != null ? !moneyAmount.equals(transfer.moneyAmount) : transfer.moneyAmount != null)
            return false;
        if (after != null ? !after.equals(transfer.after) : transfer.after != null) return false;
        if (action != null ? !action.equals(transfer.action) : transfer.action != null) return false;
        return userAccount != null ? userAccount.equals(transfer.userAccount) : transfer.userAccount == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (before != null ? before.hashCode() : 0);
        result = 31 * result + (moneyAmount != null ? moneyAmount.hashCode() : 0);
        result = 31 * result + (after != null ? after.hashCode() : 0);
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (userAccount != null ? userAccount.hashCode() : 0);
        return result;
    }
}
