package com.bestbets.app.model.support;


public enum BetMarkType {
    H0METEAM("HOMETEAM"),
    DRAW("DRAW"),
    GUESTTEAM("GUESTTEAM");

    String betMarkType;

    BetMarkType(String betMarkType) {
        this.betMarkType = betMarkType;
    }

    public String getBetMarkType() {
        return betMarkType;
    }
}
