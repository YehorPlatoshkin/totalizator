package com.bestbets.app.model.support;


public enum BetState {
    PENDING("PENDING"),
    WIN("WIN"),
    LOOSE("LOOSE");

    String betState;


    BetState(String betMarkType) {
        this.betState = betMarkType;
    }

    public String getBetState() {
        return betState;
    }
}
