package com.bestbets.app.model.support;


public enum UserProfileType {
    USER("USER"),
    MODERATOR("MODERATOR"),
    ANALYTIC("ANALYTIC"),
    ADMIN("ADMIN");


    String userProfileType;

    UserProfileType(String userProfileType) {
        this.userProfileType = userProfileType;
    }

    public String getUserProfileType() {
        return userProfileType;
    }
}
