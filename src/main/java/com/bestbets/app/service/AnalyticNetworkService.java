package com.bestbets.app.service;


import com.bestbets.app.model.AnalyticNetwork;
import com.bestbets.app.model.Sport;
import com.bestbets.app.service.support.ServiceCUDE;

public interface AnalyticNetworkService extends ServiceCUDE<AnalyticNetwork> {
    AnalyticNetwork findBySport(Sport sport);
}
