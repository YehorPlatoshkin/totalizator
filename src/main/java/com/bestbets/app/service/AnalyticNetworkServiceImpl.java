package com.bestbets.app.service;


import com.bestbets.app.dao.*;
import com.bestbets.app.model.AnalyticNetwork;
import com.bestbets.app.model.Sport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("analyticNetworkService")
@Transactional
public class AnalyticNetworkServiceImpl implements AnalyticNetworkService {
    @Autowired
    AnalyticNetworkDao analyticNetworkDao;

    @Override
    public AnalyticNetwork findBySport(Sport sport) {
        return analyticNetworkDao.findBySport(sport);
    }

    @Override
    public void create(AnalyticNetwork entity) {
        analyticNetworkDao.save(entity);
    }

    @Override
    public void update(AnalyticNetwork entity) {
        analyticNetworkDao.update(entity);
    }

    @Override
    public void delete(AnalyticNetwork entity) {
        analyticNetworkDao.delete(entity);
    }

    @Override
    public boolean isExist(AnalyticNetwork entity) {
        return analyticNetworkDao.findBySport(entity.getSport()) != null;
    }

}
