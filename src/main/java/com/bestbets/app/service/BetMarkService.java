package com.bestbets.app.service;


import com.bestbets.app.model.BetMark;

import java.util.List;

public interface BetMarkService {
    BetMark findById(Integer id);
    BetMark findByType(String type);
    List<BetMark> listAll();
}
