package com.bestbets.app.service;


import com.bestbets.app.dao.BetMarkDao;
import com.bestbets.app.model.BetMark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("betMarkService")
@Transactional
public class BetMarkServiceImpl implements BetMarkService {
    @Autowired
    BetMarkDao betMarkDao;
    @Override
    public BetMark findById(Integer id) {
        return betMarkDao.findById(id);
    }

    @Override
    public BetMark findByType(String type) {
        return betMarkDao.findByType(type);
    }

    @Override
    public List<BetMark> listAll() {
        return betMarkDao.findAll();
    }
}
