package com.bestbets.app.service;

import com.bestbets.app.model.Bet;
import com.bestbets.app.model.ResultSet;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.User;

import java.util.List;

public interface BetService {
    Bet findById(int id);
    void save(Bet entity);
    void update(Bet entity);
    boolean isBetExist(Bet bet);
    void resolveBets(SportEvent sportEvent, ResultSet resultSet);
    List<Bet> listBetsByUser(User user);
    List<Bet> listWinnByUser(User user);
    List<Bet> listLooseByUser(User user);
    List<Bet> currentBetsByUser(User user);
    List<Bet> historyBetsByUser(User user);
    Bet findBySportEventAndUser(SportEvent sportEvent, User user);
}
