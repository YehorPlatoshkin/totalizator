package com.bestbets.app.service;


import com.bestbets.app.dao.BetDao;
import com.bestbets.app.dao.EventCoefficientDao;
import com.bestbets.app.dao.SportEventDao;
import com.bestbets.app.dao.UserDao;
import com.bestbets.app.model.*;
import com.bestbets.app.model.support.BetState;
import com.bestbets.app.service.support.BetResolver;
import com.bestbets.app.service.support.WinnerLoserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("betService")
@Transactional
public class BetServiceImpl implements BetService{
    @Autowired
    BetDao betDao;
    @Autowired
    BetResolver betResolver;
    @Autowired
    EventCoefficientDao eventCoefficientDao;
    @Autowired
    UserAccountService userAccountService;
    @Autowired
    UserDao userDao;
    @Autowired
    SportEventDao sportEventDao;
    @Override
    public Bet findById(int id) {
        return betDao.findById(id);
    }

    @Override
    public void save(Bet entity) {
        betDao.save(entity);
    }

    @Override
    public void update(Bet entity) {
        betDao.update(entity);
    }

    @Override
    public boolean isBetExist(Bet bet) {
        return betDao.findBySportEventAndUser(bet.getSportEvent(), bet.getUser())!=null;
    }

    @Override
    public void resolveBets(SportEvent sportEvent, ResultSet resultSet) {
        Map<BetMark, String> betResults = betResolver.resolve(resultSet);
        for (Map.Entry<BetMark, String> betResult : betResults.entrySet()){
            if (betResult.getValue().equals(WinnerLoserType.WINNER.getType())){
                List<Bet> winnersList = betDao.findBySportEventAndBetMark(sportEvent, betResult.getKey());
                for (Bet bet :
                        winnersList) {
                }
            }else{
                List<Bet> loosersList = betDao.findBySportEventAndBetMark(sportEvent, betResult.getKey());
                for (Bet bet :
                        loosersList) {
                }
            }
        }
    }

    @Override
    public List<Bet> listBetsByUser(User user) {
        return betDao.findBetsByUser(user);
    }

    @Override
    public List<Bet> listWinnByUser(User user) {
        return betDao.findByUserAndState(user, BetState.WIN.getBetState());
    }

    @Override
    public List<Bet> listLooseByUser(User user) {
        return betDao.findByUserAndState(user,BetState.LOOSE.getBetState());
    }

    @Override
    public List<Bet> currentBetsByUser(User user) {
        return betDao.findByUserAndState(user, BetState.PENDING.getBetState());
    }

    @Override
    public List<Bet> historyBetsByUser(User user) {
        List<SportEvent> pastEvents = sportEventDao.findPastEvents();
        List<Bet> betsByUser = betDao.findBetsByUser(user);
        List<Bet> result = new ArrayList<>();
        for (Bet bet :
                betsByUser) {
            result.addAll(pastEvents.stream().filter(event -> bet.getSportEvent().equals(event))
                    .map(event -> bet).collect(Collectors.toList()));
        }
        return result;
    }

    @Override
    public Bet findBySportEventAndUser(SportEvent sportEvent, User user) {
        return betDao.findBySportEventAndUser(sportEvent,user);
    }
}
