package com.bestbets.app.service;

import com.bestbets.app.model.*;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("dataContent")
public class DataBaseContentImpl implements DataBaseContent {
    @Autowired
    TeamService teamService;
    @Autowired
    TournamentService tournamentService;
    @Autowired
    SportService sportService;
    @Autowired
    TournamentsTeamsService tournamentsTeamsService;
    @Autowired
    SportEventService sportEventService;

    @Override
    public void build() {
        createSport();
        createSportEvents();
    }
    @Override
    public void createSportEvents() {
        BufferedReader br = null;
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy hh:mm");
        try {

            String sCurrentLine;

            br = new BufferedReader(new FileReader("C:\\NeuralNetwork\\PremierLeague2014.txt"));

            while ((sCurrentLine = br.readLine()) != null) {
                String[] split = sCurrentLine.split(" ");
                String homeT = split[0];
                String homeTResult = split[1];
                String guestTResult = split[2];
                String guestT = split[3];
                SportEvent sportEvent = new SportEvent();
                sportEvent.setDateTime(new LocalDateTime(LocalDateTime.parse(split[4]+" 01:01",fmt)));
                TournamentsTeams homeTeam = findTournamentTeam(homeT);
                TournamentsTeams guestTeam = findTournamentTeam(guestT);
                sportEvent.setHomeTeam(homeTeam);
                sportEvent.setGuestTeam(guestTeam);
                ResultSet resultSet = new ResultSet();
                resultSet.setHomeTeamPoints(Integer.parseInt(homeTResult));
                resultSet.setGuestTeamPoints(Integer.parseInt(guestTResult));
                resultSet.setSportEvent(sportEvent);
                sportEvent.setResultSet(resultSet);
                sportEventService.create(sportEvent);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private TournamentsTeams findTournamentTeam(String homeT) {
        Team team = teamService.findByName(homeT);
        Tournament tournament = tournamentService.findById(1);
        return tournamentsTeamsService.singleTournamentTeam(tournament, team);
    }

    @Override
    public void createSport() {
        BufferedReader br = null;
        Set<String> teamsName = new HashSet<>();
        try {

            String sCurrentLine;

            br = new BufferedReader(new FileReader("C:\\NeuralNetwork\\PremierLeague2014.txt"));

            while ((sCurrentLine = br.readLine()) != null) {
                String[] split = sCurrentLine.split(" ");
                teamsName.add(split[0]);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        Sport sport = new Sport();
        sport.setSportName("football");
        List<Tournament> tournaments = new ArrayList<>();
        Tournament tournament = new Tournament();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy hh:mm");
        tournament.setDateTime(new LocalDateTime(LocalDateTime.parse("01/01/2014 01:01",fmt)));
        tournament.setTournamentName("PremierLeague");
        tournament.setSport(sport);
        ArrayList<TournamentsTeams> teams = new ArrayList<>();
        for (String s:
                teamsName) {
            TournamentsTeams tournTeam = new TournamentsTeams();
            Team team = new Team();
            team.setTeamName(s);
            tournTeam.setTeam(team);
            tournTeam.setTournament(tournament);
            teams.add(tournTeam);
        }
        tournament.setTeams(teams);
        tournaments.add(tournament);
        sport.setTournaments(tournaments);
        AnalyticNetwork analyticNetwork = new AnalyticNetwork();
        analyticNetwork.setNeuralNetworkPath("C:\\NeuralNetwork\\football.nnet");
        sport.setAnalyticNetwork(analyticNetwork);

        sportService.create(sport);
    }


}
