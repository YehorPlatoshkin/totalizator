package com.bestbets.app.service;

import com.bestbets.app.model.EventCoefficient;
import com.bestbets.app.model.SportEvent;

import java.util.List;

public interface EventCoefficientService {
    void setEventCoefficientForSportEvent(SportEvent sportEvent);
    void update(EventCoefficient eventCoefficient);
    List<EventCoefficient> findBySportEvent(SportEvent sportEvent);
}
