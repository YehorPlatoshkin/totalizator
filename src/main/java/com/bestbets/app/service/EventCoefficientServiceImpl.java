package com.bestbets.app.service;


import com.bestbets.app.dao.BetMarkDao;
import com.bestbets.app.dao.EventCoefficientDao;
import com.bestbets.app.model.BetMark;
import com.bestbets.app.model.EventCoefficient;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.support.BetMarkType;
import com.bestbets.app.service.managers.AnalyticManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
@Service("eventCoefficientService")
@Transactional
public class EventCoefficientServiceImpl implements EventCoefficientService {
    @Autowired
    EventCoefficientDao eventCoefficientDao;
    @Autowired
    AnalyticManager analyticManager;
    @Autowired
    BetMarkDao betMarkDao;


     @Override
        public void setEventCoefficientForSportEvent(SportEvent sportEvent) {
            double[] probabilityBySportEvent = analyticManager.calculateProbabilityBySportEvent(sportEvent);
            double homeTeamCoefficient =probabilityBySportEvent[0]*100;
            double drawCoefficient = probabilityBySportEvent[1]*100;
            double guestTeamCoefficient = probabilityBySportEvent[2]*100;
            BetMark homeTeamMark = betMarkDao.findByType(BetMarkType.H0METEAM.getBetMarkType());
            BetMark drawMark = betMarkDao.findByType(BetMarkType.DRAW.getBetMarkType());
            BetMark guestTeamMark = betMarkDao.findByType(BetMarkType.GUESTTEAM.getBetMarkType());
            EventCoefficient homeTeam = new EventCoefficient();
            homeTeam.setBetMark(homeTeamMark);
            homeTeam.setCoefficient(new BigDecimal(homeTeamCoefficient).setScale(2, RoundingMode.UP));
            homeTeam.setSportEvent(sportEvent);
            eventCoefficientDao.save(homeTeam);
            EventCoefficient draw = new EventCoefficient();
            draw.setBetMark(drawMark);
            draw.setCoefficient(new BigDecimal(drawCoefficient).setScale(2, RoundingMode.UP));
            draw.setSportEvent(sportEvent);
            eventCoefficientDao.save(draw);
            EventCoefficient guestTeam = new EventCoefficient();
            guestTeam.setBetMark(guestTeamMark);
            guestTeam.setCoefficient(new BigDecimal(guestTeamCoefficient).setScale(2, RoundingMode.UP));
            guestTeam.setSportEvent(sportEvent);
            eventCoefficientDao.save(guestTeam);
        }
    @Override
    public void update(EventCoefficient eventCoefficient) {
        eventCoefficientDao.update(eventCoefficient);
    }

    @Override
    public List<EventCoefficient> findBySportEvent(SportEvent sportEvent) {
        return eventCoefficientDao.findBySportEvent(sportEvent);
    }
}

