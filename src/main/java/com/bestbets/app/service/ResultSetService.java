package com.bestbets.app.service;


import com.bestbets.app.model.ResultSet;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.service.support.ServiceCUDE;

public interface ResultSetService extends ServiceCUDE<ResultSet>{
    ResultSet findBySpotEvent(SportEvent sportEvent);
}
