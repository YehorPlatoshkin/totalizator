package com.bestbets.app.service;


import com.bestbets.app.dao.*;
import com.bestbets.app.model.ResultSet;
import com.bestbets.app.model.SportEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("resultSetService")
@Transactional
public class ResultSetServiceImpl implements ResultSetService {
    @Autowired
    ResultSetDao resultSetDao;

    @Override
    public ResultSet findBySpotEvent(SportEvent sportEvent) {
        return resultSetDao.findBySpotEvent(sportEvent);
    }

    @Override
    public void create(ResultSet resultSet) {
        resultSetDao.save(resultSet);
    }

    @Override
    public void update(ResultSet resultSet) {
        resultSetDao.update(resultSet);
    }

    @Override
    public void delete(ResultSet entity) {
        resultSetDao.delete(entity);
    }

    @Override
    public boolean isExist(ResultSet entity) {
        return resultSetDao.findBySpotEvent(entity.getSportEvent()) != null;
    }


}
