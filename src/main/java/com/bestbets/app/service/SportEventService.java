package com.bestbets.app.service;


import com.bestbets.app.model.Sport;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.model.TournamentsTeams;
import com.bestbets.app.service.support.ServiceCUDE;
import org.joda.time.LocalDateTime;

import java.util.List;
import java.util.Set;

public interface SportEventService extends ServiceCUDE<SportEvent>{
    SportEvent findById(Integer id);
    SportEvent findByMeeting(TournamentsTeams homeTeam, TournamentsTeams guestTeam, LocalDateTime dateTime);

    /*Current*/
    List<SportEvent> findCurrentEvents();

    List<SportEvent> findCurrentEventsByTeam(TournamentsTeams team);

    /*Past*/
    List<SportEvent> findPastEvents();

    List<SportEvent> findPastEventsByTeam(TournamentsTeams team, LocalDateTime dateTime);

    @SuppressWarnings("unchecked")
    List<SportEvent> findPastEventsByHomeTeam(TournamentsTeams team, LocalDateTime dateTime);

    @SuppressWarnings("unchecked")
    List<SportEvent> findPastEventsByGuestTeam(TournamentsTeams team, LocalDateTime dateTime);

    /*Temporary*/
    List<SportEvent> findAllOrderByDate();

    Set<SportEvent> findAllByTournament(Tournament tournament);

    Set<SportEvent> findAllBySport(Sport sport);
    List<SportEvent> findAllByTeam(TournamentsTeams teams);
}
