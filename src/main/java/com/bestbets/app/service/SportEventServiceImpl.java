package com.bestbets.app.service;


import com.bestbets.app.dao.SportEventDao;
import com.bestbets.app.dao.TournamentDao;
import com.bestbets.app.dao.TournamentsTeamsDao;
import com.bestbets.app.model.Sport;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.model.TournamentsTeams;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("sportEventService")
@Transactional
public class SportEventServiceImpl implements SportEventService {
    @Autowired
    SportEventDao sportEventDao;
    @Autowired
    TournamentsTeamsDao tournamentsTeamsDao;
    @Autowired
    TournamentDao tournamentDao;

    @Override
    public SportEvent findById(Integer id) {
        return sportEventDao.findById(id);
    }

    @Override
    public SportEvent findByMeeting(TournamentsTeams homeTeam, TournamentsTeams guestTeam, LocalDateTime dateTime) {
        return sportEventDao.findByMeeting(homeTeam,guestTeam,dateTime);
    }

    @Override
    public List<SportEvent> findCurrentEvents(){
        return sportEventDao.findCurrentEvents();
    }

    @Override
    public List<SportEvent> findCurrentEventsByTeam(TournamentsTeams team) {
        return sportEventDao.findCurrentEventsByTeam(team);
    }

    @Override
    public List<SportEvent> findPastEvents(){
        return sportEventDao.findPastEvents();
    }


    @Override
    public List<SportEvent> findPastEventsByTeam(TournamentsTeams team, LocalDateTime dateTime) {
        return sportEventDao.findPastEventsByTeam(team, dateTime);
    }

    @Override
    public List<SportEvent> findPastEventsByHomeTeam(TournamentsTeams team, LocalDateTime dateTime) {
        return sportEventDao.findPastEventsByHomeTeam(team, dateTime);
    }

    @Override
    public List<SportEvent> findPastEventsByGuestTeam(TournamentsTeams team, LocalDateTime dateTime) {
        return sportEventDao.findPastEventsByGuestTeam(team, dateTime);
    }

    @Override
    public List<SportEvent> findAllOrderByDate() {
        return sportEventDao.findAllOrderByDate();
    }

    @Override
    public Set<SportEvent> findAllByTournament(Tournament tournament) {
        List<TournamentsTeams> teamsList = tournamentsTeamsDao.findByTournament(tournament);
        Set<SportEvent> sportEventSet = new HashSet<>();
        for (TournamentsTeams team :
                teamsList) {
            List<SportEvent> sportEventsByTeam = sportEventDao.findByTeam(team);
            sportEventSet.addAll(sportEventsByTeam);
        }
        return sportEventSet;
    }

    @Override
    public Set<SportEvent> findAllBySport(Sport sport) {
        List<Tournament> tournamentList = tournamentDao.findBySport(sport);
        Set<SportEvent> sportEventSet = new HashSet<>();
        for (Tournament tournament:
                tournamentList) {
            List<TournamentsTeams> teamsList = tournamentsTeamsDao.findByTournament(tournament);
            for (TournamentsTeams team :
                    teamsList) {
                List<SportEvent> sportEventsByTeam = sportEventDao.findByTeam(team);
                sportEventSet.addAll(sportEventsByTeam);
            }
        }
        return sportEventSet;
    }

    @Override
    public List<SportEvent> findAllByTeam(TournamentsTeams teams) {
        return sportEventDao.findByTeam(teams);
    }

    @Override
    public void create(SportEvent sportEvent) {
        sportEventDao.save(sportEvent);

    }

    @Override
    public void update(SportEvent sportEvent) {
        sportEventDao.update(sportEvent);
    }

    @Override
    public void delete(SportEvent entity) {
        sportEventDao.delete(entity);
    }

    @Override
    public boolean isExist(SportEvent entity) {
        return sportEventDao.findByMeeting(entity.getHomeTeam(),
                entity.getGuestTeam(),
                entity.getDateTime())!=null;
    }
}

