package com.bestbets.app.service;

import com.bestbets.app.model.Sport;
import com.bestbets.app.service.support.ServiceCUDE;

import java.util.List;

public interface SportService extends ServiceCUDE<Sport> {
    Sport findById(Integer id);
    Sport findByName(String name);
    List<Sport> findAll();
}
