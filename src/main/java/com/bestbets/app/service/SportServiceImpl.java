package com.bestbets.app.service;


import com.bestbets.app.dao.SportDao;
import com.bestbets.app.model.Sport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("sportService")
@Transactional
public class SportServiceImpl implements SportService{
    @Autowired
    SportDao sportDao;

    @Override
    public Sport findById(Integer id) {
        return sportDao.findById(id);
    }

    @Override
    public Sport findByName(String name) {
        return sportDao.findByName(name);
    }

    @Override
    public List<Sport> findAll() {
        return sportDao.findAll();
    }

    @Override
    public void create(Sport entity) {
        sportDao.save(entity);
    }

    @Override
    public void update(Sport entity) {
        sportDao.update(entity);
    }

    @Override
    public void delete(Sport entity) {
        sportDao.delete(entity);
    }

    @Override
    public boolean isExist(Sport entity) {
        return sportDao.findByName(entity.getSportName()) !=null;
    }
}
