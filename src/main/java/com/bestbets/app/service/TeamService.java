package com.bestbets.app.service;


import com.bestbets.app.model.Team;
import com.bestbets.app.service.support.ServiceCUDE;

public interface TeamService extends ServiceCUDE<Team> {
    Team findById(Integer id);
    Team findByName(String name);
}
