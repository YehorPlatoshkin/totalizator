package com.bestbets.app.service;


import com.bestbets.app.dao.TeamDao;
import com.bestbets.app.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("teamService")
@Transactional
public class TeamServiceImpl implements TeamService{
    @Autowired
    TeamDao teamDao;

    @Override
    public Team findById(Integer id) {
        return teamDao.findById(id);
    }

    @Override
    public Team findByName(String name) {
        return teamDao.findByName(name);
    }

    @Override
    public void create(Team team) {
        teamDao.save(team);
    }

    @Override
    public void update(Team team) {
        teamDao.update(team);
    }

    @Override
    public void delete(Team team) {
        teamDao.delete(team);
    }

    @Override
    public boolean isExist(Team entity) {
        return teamDao.findByName(entity.getTeamName())!=null;
    }

}
