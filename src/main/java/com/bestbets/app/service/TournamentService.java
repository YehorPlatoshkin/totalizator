package com.bestbets.app.service;


import com.bestbets.app.model.Sport;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.service.support.ServiceCUDE;
import org.joda.time.LocalDateTime;

import java.util.List;

public interface TournamentService extends ServiceCUDE<Tournament>{
    Tournament findById(Integer id);
    Tournament findByNameAndYear(String name, LocalDateTime date);
    List<Tournament> findBySport(Sport sport);
}
