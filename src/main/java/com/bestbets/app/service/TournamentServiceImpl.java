package com.bestbets.app.service;


import com.bestbets.app.dao.TournamentDao;
import com.bestbets.app.model.Sport;
import com.bestbets.app.model.Tournament;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("tournamentService")
@Transactional
public class TournamentServiceImpl implements TournamentService {
    @Autowired
    TournamentDao tournamentDao;


    @Override
    public Tournament findById(Integer id) {
        return tournamentDao.findById(id);
    }

    @Override
    public Tournament findByNameAndYear(String name, LocalDateTime date) {
        return tournamentDao.findByNameAndYear(name,date);
    }

    @Override
    public List<Tournament> findBySport(Sport sport) {
        return tournamentDao.findBySport(sport);
    }

    @Override
    public void create(Tournament tournament) {
        tournamentDao.save(tournament);
    }

    @Override
    public void update(Tournament tournament) {
        tournamentDao.update(tournament);
    }

    @Override
    public void delete(Tournament tournament) {
        tournamentDao.delete(tournament);
    }

    @Override
    public boolean isExist(Tournament entity) {
        return findByNameAndYear(entity.getTournamentName(), entity.getDateTime())!=null;
    }

}
