package com.bestbets.app.service;


import com.bestbets.app.model.Team;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.model.TournamentsTeams;
import com.bestbets.app.service.support.ServiceCUDE;

import java.util.List;

public interface TournamentsTeamsService extends ServiceCUDE<TournamentsTeams> {
    List<TournamentsTeams> listTeamsByTournament(Tournament tournament);
    TournamentsTeams singleTournamentTeam(Tournament tournament, Team team);
    TournamentsTeams singleTournamentTeamById(Integer id);
    List<TournamentsTeams> listByTeam(Team team);
}
