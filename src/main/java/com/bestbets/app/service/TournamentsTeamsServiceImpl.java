package com.bestbets.app.service;

import com.bestbets.app.dao.TournamentsTeamsDao;
import com.bestbets.app.model.Team;
import com.bestbets.app.model.Tournament;
import com.bestbets.app.model.TournamentsTeams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("tournamentsTeamsService")
@Transactional
public class TournamentsTeamsServiceImpl implements TournamentsTeamsService{
    @Autowired
    TournamentsTeamsDao tournamentsTeamsDao;

    @Override
    public List<TournamentsTeams> listTeamsByTournament(Tournament tournament) {
        return tournamentsTeamsDao.findByTournament(tournament);
    }

    @Override
    public TournamentsTeams singleTournamentTeam(Tournament tournament, Team team) {
        return tournamentsTeamsDao.findSingleTournamenTeam(tournament,team);
    }

    @Override
    public TournamentsTeams singleTournamentTeamById(Integer id) {
        return tournamentsTeamsDao.findById(id);
    }

    @Override
    public List<TournamentsTeams> listByTeam(Team team) {
        return tournamentsTeamsDao.findByTeam(team);
    }

    @Override
    public void create(TournamentsTeams entity) {
        tournamentsTeamsDao.save(entity);
    }

    @Override
    public void update(TournamentsTeams entity) {
        tournamentsTeamsDao.update(entity);
    }

    @Override
    public void delete(TournamentsTeams entity) {
        tournamentsTeamsDao.delete(entity);
    }

    @Override
    public boolean isExist(TournamentsTeams entity) {
        return tournamentsTeamsDao.findSingleTournamenTeam(entity.getTournament(), entity.getTeam())!= null;
    }
}
