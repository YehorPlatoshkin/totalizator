package com.bestbets.app.service;


import com.bestbets.app.model.Transfer;
import com.bestbets.app.model.UserAccount;

import java.util.List;

public interface TransferService {
    List<Transfer> listByUserAccount(UserAccount userAccount);
}
