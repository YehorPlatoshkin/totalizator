package com.bestbets.app.service;

import com.bestbets.app.dao.TransferDao;
import com.bestbets.app.model.Transfer;
import com.bestbets.app.model.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("transferService")
@Transactional
public class TransferServiceImpl implements TransferService{
    @Autowired
    TransferDao transferDao;

    @Override
    public List<Transfer> listByUserAccount(UserAccount userAccount) {
        return transferDao.findByUserAccount(userAccount);
    }
}
