package com.bestbets.app.service;

import com.bestbets.app.model.User;
import com.bestbets.app.model.UserAccount;

import java.math.BigDecimal;
import java.util.List;

public interface UserAccountService {
    void create(UserAccount userAccount);
    void update(UserAccount userAccount);
    void delete(UserAccount userAccount);
    List<UserAccount> findAll();
    UserAccount findByUser(User user);
    UserAccount findById(Integer id);
    void putMoneyToUser(UserAccount userAccount, BigDecimal moneyAmount);
    void getMoneyFromUser(UserAccount userAccount, BigDecimal moneyAmount);
}
