package com.bestbets.app.service;

import com.bestbets.app.dao.UserAccountDao;
import com.bestbets.app.model.User;
import com.bestbets.app.model.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service("userAccountService")
@Transactional
public class UserAccountServiceImpl implements UserAccountService {
    @Autowired
    UserAccountDao userAccountDao;

    @Override
    public void create(UserAccount userAccount) {
        userAccountDao.save(userAccount);
    }

    @Override
    public void update(UserAccount userAccount) {
        userAccountDao.update(userAccount);
    }

    @Override
    public void delete(UserAccount userAccount) {
        userAccountDao.delete(userAccount);
    }

    @Override
    public List<UserAccount> findAll() {
        return userAccountDao.findAll();
    }

    @Override
    public UserAccount findByUser(User user) {
        return userAccountDao.findByUser(user);
    }

    @Override
    public UserAccount findById(Integer id) {
        return userAccountDao.findById(id);
    }

    @Override
    public void putMoneyToUser(UserAccount userAccount, BigDecimal moneyAmount) {
        BigDecimal currentBalance = userAccount.getBalance();
        userAccount.setBalance(currentBalance.add(moneyAmount));
        userAccountDao.update(userAccount);
    }

    @Override
    public void getMoneyFromUser(UserAccount userAccount, BigDecimal moneyAmount) {
        BigDecimal currentBalance = userAccount.getBalance();
        userAccount.setBalance(currentBalance.subtract(moneyAmount));
        userAccountDao.update(userAccount);
    }
}
