package com.bestbets.app.service;

import com.bestbets.app.model.UserProfile;

import java.util.List;

public interface UserProfileService {
    UserProfile findById(Integer id);
    List<UserProfile> listAll();
    UserProfile findByType(String type);

}
