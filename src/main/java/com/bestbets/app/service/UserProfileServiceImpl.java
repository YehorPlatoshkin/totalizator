package com.bestbets.app.service;


import com.bestbets.app.dao.UserProfileDao;
import com.bestbets.app.model.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService {
    @Autowired
    UserProfileDao userProfileDao;

    @Override
    public UserProfile findById(Integer id) {
        return userProfileDao.findById(id);
    }

    @Override
    public List<UserProfile> listAll() {
        return userProfileDao.findAll();
    }

    @Override
    public UserProfile findByType(String type) {
        return userProfileDao.findByType(type);
    }
}
