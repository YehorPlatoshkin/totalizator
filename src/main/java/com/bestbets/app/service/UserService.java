package com.bestbets.app.service;


import com.bestbets.app.model.User;

import java.util.List;

public interface UserService {
    User findById(Integer id);
    void create(User user);
    void update(User user);
    void deleteById(Integer id);
    boolean isUserExist(User user);
    User findByEmail(String email);
    List<User> listWorkingStuff();
}
