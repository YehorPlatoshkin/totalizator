package com.bestbets.app.service;

import com.bestbets.app.dao.UserDao;
import com.bestbets.app.dao.UserProfileDao;
import com.bestbets.app.model.*;
import com.bestbets.app.model.support.State;
import com.bestbets.app.model.support.UserProfileType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    UserDao userDao;

    @Autowired
    UserProfileDao userProfileDao;

    @Override
    public User findById(Integer id) {
        return userDao.findById(id);
    }

    @Override
    public void create(User user) {
        if (user.getProfile()== null){
            UserProfile userType = userProfileDao.findByType(UserProfileType.USER.getUserProfileType());
            user.setProfile(userType);
            UserAccount userAccount = new UserAccount();
            userAccount.setUser(user);
            user.setAccount(userAccount);
        }
        userDao.save(user);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public void deleteById(Integer id) {
        User entity  = userDao.findById(id);
        entity.setState(State.DELETED.getState());
        userDao.update(entity);
    }

    /*@Override
    public void inactiveUser(User user) {
        User entity  = userDao.findById(user.getId());
        entity.setState(State.INACTIVE.getState());
        userDao.update(user);
    }

    @Override
    public void lockUser(User user) {
        User entity  = userDao.findById(user.getId());
        entity.setState(State.LOCKED.getState());
        userDao.update(user);
    }*/
    @Override
    public boolean isUserExist(User user) {
        return userDao.findByEmail(user.getEmail())!=null;
    }
    @Override
    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }


    @Override
    public List<User> listWorkingStuff() {
        UserProfile moderatorProfile = userProfileDao.findByType(UserProfileType.MODERATOR.getUserProfileType());
        UserProfile analiticProfile = userProfileDao.findByType(UserProfileType.ANALYTIC.getUserProfileType());
        List<User> moderators = userDao.findByProfile(moderatorProfile);
        List<User> analitics = userDao.findByProfile(analiticProfile);
        List<User> workingStuff = new ArrayList<>();
        workingStuff.addAll(moderators);
        workingStuff.addAll(analitics);
        return workingStuff;
    }
}
