package com.bestbets.app.service.managers;

import com.bestbets.app.model.Sport;
import com.bestbets.app.model.SportEvent;

import java.util.List;

public interface AnalyticManager {
    double[] calculateProbabilityBySportEvent(SportEvent sportEvent);
    List<Double> parametersForView(SportEvent sportEvent);
    void createNeuralNetworkBySport(Sport sport);

    double[] calculateSportEventParameters(SportEvent sportEvent);
}
