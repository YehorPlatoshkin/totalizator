package com.bestbets.app.service.managers;

import com.bestbets.app.model.*;
import com.bestbets.app.service.AnalyticNetworkService;
import com.bestbets.app.service.ResultSetService;
import com.bestbets.app.service.SportEventService;
import com.bestbets.app.service.TournamentsTeamsService;
import com.bestbets.app.util.Rating;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.joda.time.LocalDateTime;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.util.TransferFunctionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Service("analyticManager")
public class AnalyticManagerImpl implements AnalyticManager {
    private static final Integer MAGICNUMBER = 106;
    @Autowired
    AnalyticNetworkService analyticNetworkService;

    @Autowired
    SportEventService sportEventService;

    @Autowired
    TournamentsTeamsService tournamentsTeamsService;
    @Autowired
    ResultSetService resultSetService;

    @Override
    public double[] calculateProbabilityBySportEvent(SportEvent sportEvent) {
        Sport sport = sportEvent.getHomeTeam().getTournament().getSport();
        AnalyticNetwork analyticNetwork = analyticNetworkService.findBySport(sport);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(analyticNetwork.getNeuralNetworkPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        MultiLayerPerceptron neuralNet = (MultiLayerPerceptron) NeuralNetwork.load(inputStream);
        double[] sportEventParameters = calculateSportEventParameters(sportEvent);
        neuralNet.setInput(sportEventParameters);
        neuralNet.calculate();
        return neuralNet.getOutput();
    }

    @Override
    public List<Double> parametersForView(SportEvent sportEvent) {
        List<Double> viewOutput = new ArrayList<>();
        double[] doubleProbability = calculateSportEventParameters(sportEvent);
        for (double v :
                doubleProbability) {
            viewOutput.add(v);
        }
        return viewOutput;
    }

    @Override
    public void createNeuralNetworkBySport(Sport sport)  {
        int inputsCount = 8;
        int outputsCount = 3;
        /*XLSX*/
        //Blank workbook
        HSSFWorkbook workbook = new HSSFWorkbook();
        //Create a blank sheet
        HSSFSheet sheet = workbook.createSheet("Training Data");
        /*File*/
        StringBuilder stringBuilder = new StringBuilder();
        DataSet trainingSet = new DataSet(8, 3);
        Set<SportEvent> sportEventDaoAll = sportEventService.findAllBySport(sport);
        int rownum = 0;
        Integer counter = 0;
        for (SportEvent sportEvent :
                sportEventDaoAll) {
            if (counter < MAGICNUMBER) {
            /*XLSX*/
                Row row = sheet.createRow(rownum++);
                double[] input = calculateSportEventParameters(sportEvent);
                double[] desiredOutput = resultSetResolve(sportEvent);
                Cell cell = row.createCell(0);
                cell.setCellValue(sportEvent.getId());
                int factorNum = 1;
                for (double v :
                        input) {
                    Cell rowCell = row.createCell(factorNum++);
                    rowCell.setCellValue(v);
                }
                int resultNum = 10;
                for (double v :
                        desiredOutput) {
                    Cell rowCell = row.createCell(resultNum++);
                    rowCell.setCellValue(v);
                }
            /*File*/
                stringBuilder.append(Arrays.toString(input));
                stringBuilder.append(Arrays.toString(desiredOutput));
                stringBuilder.append("\n");
            /*NeuralNet*/
                DataSetRow dataSetRow = new DataSetRow(input, desiredOutput);
                trainingSet.addRow(dataSetRow);
            }
            counter++;
        }
        String toWrite = stringBuilder.toString().replaceAll("\\]\\[",",");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("C:\\NeuralNetwork\\Training.txt"));
            writer.write(toWrite);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("C:\\NeuralNetwork\\Training_" + sport.getSportName() + ".xls"));
            workbook.write(out);
            out.close();
            System.out.println("Training_" + sport.getSportName() + " written successfully on disk.");
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Creating neural network");
        MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, 8, 30, 3);

        MomentumBackpropagation learningRule = (MomentumBackpropagation) neuralNet.getLearningRule();
        learningRule.setLearningRate(0.3);
        learningRule.setMomentum(0.7);

        String filePathToNeuralNet = "C:\\NeuralNetwork\\" + sport.getSportName() + ".nnet";
        System.out.println("Training neural network...");
        neuralNet.learn(trainingSet);
        neuralNet.save(filePathToNeuralNet);
        AnalyticNetwork analyticNetwork = new AnalyticNetwork();
        analyticNetwork.setNeuralNetworkPath(filePathToNeuralNet);
        analyticNetwork.setSport(sport);
        analyticNetworkService.create(analyticNetwork);
    }

    @Override
    public double[] calculateSportEventParameters(SportEvent sportEvent) {
        TournamentsTeams homeTeam = sportEvent.getHomeTeam();
        TournamentsTeams guestTeam = sportEvent.getGuestTeam();
        LocalDateTime sportEventDate = sportEvent.getDateTime();
        List<SportEvent> homeTeamPastEvents = sportEventService.findPastEventsByTeam(homeTeam, sportEventDate).size() > 5
                ? sportEventService.findPastEventsByTeam(homeTeam, sportEventDate)
                : searchAvailableSelection(homeTeam, sportEventDate);
        List<SportEvent> guestTeamPastEvents = sportEventService.findPastEventsByTeam(guestTeam, sportEventDate).size() > 5
                ? sportEventService.findPastEventsByTeam(guestTeam, sportEventDate)
                : searchAvailableSelection(guestTeam, sportEventDate);
        homeTeam = check(homeTeam, homeTeamPastEvents);
        guestTeam = check(guestTeam, guestTeamPastEvents);
        double homeTeamAttack = Rating.calculateAttack(homeTeamPastEvents, homeTeam);
        double homeTeamDefense = Rating.calculateDefense(homeTeamPastEvents, homeTeam);
        double guestTeamAttack = Rating.calculateAttack(guestTeamPastEvents, guestTeam);
        double guestTeamDefense = Rating.calculateDefense(guestTeamPastEvents, guestTeam);
        List<TournamentsTeams> homeTeamTournamentTable = tournamentsTeamsService.listTeamsByTournament(homeTeam.getTournament());
        List<TournamentsTeams> guestTeamTournamentTable = tournamentsTeamsService.listTeamsByTournament(guestTeam.getTournament());
        double homeTeamTournamentPosition = Rating.calculateTournament(homeTeamTournamentTable, homeTeam);
        double guestTeamTournamntPosition = Rating.calculateTournament(guestTeamTournamentTable, guestTeam);
        List<SportEvent> pastEventsByHomeTeam = sportEventService.findPastEventsByHomeTeam(homeTeam, sportEventDate);
        List<SportEvent> pastEventsByGuestTeam = sportEventService.findPastEventsByGuestTeam(guestTeam, sportEventDate);
        double fieldFactorHomeTeam = Rating.fieldFactorHomeTeam(pastEventsByHomeTeam);
        double fieldFactorGuestTeam = Rating.fieldFactorGuestTeam(pastEventsByGuestTeam);
        System.out.println(homeTeamAttack + " " + homeTeamDefense + " " + homeTeamTournamentPosition + " " + fieldFactorHomeTeam + "\n" +
                " " + guestTeamAttack + " " + guestTeamDefense + " " + guestTeamTournamntPosition + " " + fieldFactorGuestTeam);
        return new double[]{homeTeamAttack, homeTeamDefense, homeTeamTournamentPosition, fieldFactorHomeTeam,
                guestTeamAttack, guestTeamDefense, guestTeamTournamntPosition, fieldFactorGuestTeam};

    }

    private double[] resultSetResolve(SportEvent sportEvent) {
        ResultSet resultSet = resultSetService.findBySpotEvent(sportEvent);
        double[] result = new double[]{0.0, 0.0, 0.0};
        if (resultSet.getHomeTeamPoints() > resultSet.getGuestTeamPoints()) {
            result[0] = 1.0;
        } else if (resultSet.getHomeTeamPoints() < resultSet.getGuestTeamPoints()) {
            result[2] = 1.0;
        } else {
            result[1] = 1.0;
        }
        System.out.println(Arrays.toString(result));
        return result;
    }

    private TournamentsTeams check(TournamentsTeams team, List<SportEvent> pastEvents) {
        Team necessaryTeam = team.getTeam();
        if (!pastEvents.isEmpty()) {
            SportEvent anyEvent = pastEvents.get(0);
            Tournament tournament = anyEvent.getHomeTeam().getTournament();
            if (!anyEvent.getHomeTeam().equals(team) || !anyEvent.getGuestTeam().equals(team)) {
                team = tournamentsTeamsService.singleTournamentTeam(tournament, necessaryTeam);
            }
        }
        return team;
    }


    public List<SportEvent> searchAvailableSelection(TournamentsTeams team, LocalDateTime dateTime) {
        Team teamObj = team.getTeam();
        List<TournamentsTeams> teams = tournamentsTeamsService.listByTeam(teamObj);
        int size = teams.size();
        int index = 0;
        List<SportEvent> pastEventsByTeam;
        do {
            pastEventsByTeam = sportEventService.findPastEventsByTeam(teams.get(index), dateTime);
            index++;
        } while (--size > 0 || pastEventsByTeam.size() > 5);
        return pastEventsByTeam;
    }

}
