package com.bestbets.app.service.managers;


import com.bestbets.app.model.ResultSet;
import com.bestbets.app.model.SportEvent;

public interface SportEventManager {
    void openSportEvent(SportEvent sportEvent);

    void closeSportEvent(ResultSet resultSet);
}
