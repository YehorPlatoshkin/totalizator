package com.bestbets.app.service.managers;

import com.bestbets.app.model.ResultSet;
import com.bestbets.app.model.SportEvent;
import com.bestbets.app.service.BetService;
import com.bestbets.app.service.EventCoefficientService;
import com.bestbets.app.service.ResultSetService;
import com.bestbets.app.service.SportEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("sportEventManager")
public class SportEventManagerImpl implements SportEventManager {
    @Autowired
    SportEventService sportEventService;

    @Autowired
    ResultSetService resultSetService;

    @Autowired
    EventCoefficientService eventCoefficientService;

    @Autowired
    BetService betService;

    @Override
    public void openSportEvent(SportEvent sportEvent){
        sportEventService.create(sportEvent);
        eventCoefficientService.setEventCoefficientForSportEvent(sportEvent);
    }

    @Override
    public void closeSportEvent(ResultSet resultSet){
        resultSetService.create(resultSet);
        betService.resolveBets(resultSet.getSportEvent(), resultSet);
    }

}
