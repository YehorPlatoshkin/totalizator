package com.bestbets.app.service.support;


import com.bestbets.app.model.BetMark;
import com.bestbets.app.model.ResultSet;

import java.util.Map;

public interface BetResolver {
    Map<BetMark, String> resolve(ResultSet resultSet);
}
