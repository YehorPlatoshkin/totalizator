package com.bestbets.app.service.support;

import com.bestbets.app.dao.BetMarkDao;
import com.bestbets.app.model.BetMark;
import com.bestbets.app.model.support.BetMarkType;
import com.bestbets.app.model.ResultSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class BetResolverImpl implements BetResolver {
    @Autowired
    BetMarkDao betMarkDao;

    @Override
    public Map<BetMark, String> resolve(ResultSet resultSet) {
        Map<BetMark, String> map = new HashMap<>();
        BetMark winnerBetMark;
        BetMark loser1BetMark;
        BetMark loser2BetMark;
        if (resultSet.getHomeTeamPoints() > resultSet.getGuestTeamPoints()) {
            winnerBetMark = betMarkDao.findByType(BetMarkType.H0METEAM.getBetMarkType());
            loser1BetMark = betMarkDao.findByType(BetMarkType.GUESTTEAM.getBetMarkType());
            loser2BetMark = betMarkDao.findByType(BetMarkType.DRAW.getBetMarkType());
            map.put(winnerBetMark, WinnerLoserType.WINNER.getType());
            map.put(loser1BetMark, WinnerLoserType.LOSER.getType());
            map.put(loser2BetMark, WinnerLoserType.LOSER.getType());
        } else if (resultSet.getHomeTeamPoints() < resultSet.getGuestTeamPoints()) {
            winnerBetMark = betMarkDao.findByType(BetMarkType.GUESTTEAM.getBetMarkType());
            loser1BetMark = betMarkDao.findByType(BetMarkType.H0METEAM.getBetMarkType());
            loser2BetMark = betMarkDao.findByType(BetMarkType.DRAW.getBetMarkType());
            map.put(winnerBetMark, WinnerLoserType.WINNER.getType());
            map.put(loser1BetMark, WinnerLoserType.LOSER.getType());
            map.put(loser2BetMark, WinnerLoserType.LOSER.getType());
        } else if (resultSet.getHomeTeamPoints().equals(resultSet.getGuestTeamPoints())) {
            winnerBetMark = betMarkDao.findByType(BetMarkType.DRAW.getBetMarkType());
            loser1BetMark = betMarkDao.findByType(BetMarkType.GUESTTEAM.getBetMarkType());
            loser2BetMark = betMarkDao.findByType(BetMarkType.H0METEAM.getBetMarkType());
            map.put(winnerBetMark, WinnerLoserType.WINNER.getType());
            map.put(loser1BetMark, WinnerLoserType.LOSER.getType());
            map.put(loser2BetMark, WinnerLoserType.LOSER.getType());
        }
        return map;
    }
}
