package com.bestbets.app.service.support;


public interface ServiceCUDE<T>{
    void create(T entity);
    void update(T entity);
    void delete(T entity);
    boolean isExist(T entity);
}
