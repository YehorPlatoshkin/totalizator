package com.bestbets.app.service.support;


public enum WinnerLoserType {
    WINNER("WINNER"),
    LOSER("LOSER");

    String Type;

    WinnerLoserType(String Type) {
        this.Type = Type;
    }

    public String getType() {
        return Type;
    }
}
