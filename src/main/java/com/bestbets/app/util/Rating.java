package com.bestbets.app.util;

import com.bestbets.app.model.SportEvent;
import com.bestbets.app.model.TournamentsTeams;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class Rating {
    public static Double calculateAttack(List<SportEvent> sportEvents, TournamentsTeams team) {
        Double rating = 0.0;
        Integer gamePoints = 0;
        Integer min = 0;
        Integer max = 3;
        for (SportEvent sportEvent :
                sportEvents) {
            if (sportEvent.getHomeTeam().equals(team)) {
                gamePoints += sportEvent.getResultSet().getHomeTeamPoints();
            } else {
                gamePoints += sportEvent.getResultSet().getGuestTeamPoints();
            }
        }
        rating = (double) (gamePoints - min) / (double) (max * sportEvents.size() - min);
        return rating.isNaN()? 0.0 : new BigDecimal(rating).setScale(4, RoundingMode.UP).doubleValue();
    }

    public static Double calculateDefense(List<SportEvent> sportEvents,TournamentsTeams team) {
        Double rating = 0.0;
        Integer gamePoints = 0;
        Integer min = 0;
        Integer max = 3;
        for (SportEvent sportEvent :
                sportEvents) {
            if (sportEvent.getHomeTeam().equals(team)) {
                gamePoints += sportEvent.getResultSet().getGuestTeamPoints();
            } else {
                gamePoints += sportEvent.getResultSet().getHomeTeamPoints();
            }
        }
        rating = (((double) (gamePoints - min) / (double) (max * sportEvents.size() - min)) - 1.0) * (-1.0);
        return rating.isNaN()? 0.0 : new BigDecimal(rating).setScale(4, RoundingMode.UP).doubleValue();
    }

    public static double calculateTournament(List<TournamentsTeams> tournamentList, TournamentsTeams team) {
        Integer tournamentPosition = tournamentList.indexOf(team);
        Double rating;
        double min = 0.0;
        Integer max = tournamentList.size();
        rating = (((double) (tournamentPosition - min) / (double) (max - min)) - 1.0) * (-1.0);
        return rating.isNaN() || rating < 0.0? 0.0 : new BigDecimal(rating).setScale(4, RoundingMode.UP).doubleValue();
    }

    public static double fieldFactorHomeTeam(List<SportEvent> sportEvents) {
        Double rating = 0.0;
        Integer gamesPoints = 0;
        Integer min = 0;
        Integer max = 3;
        for (SportEvent sportEvent : sportEvents) {
            gamesPoints += sportEvent.getResultSet().getHomeTeamPoints();
        }
        rating = ((double) (gamesPoints - min) / (double) (max*sportEvents.size() - min));
        return rating.isNaN()? 0.0 : new BigDecimal(rating).setScale(4, RoundingMode.UP).doubleValue();
    }

    public static double fieldFactorGuestTeam(List<SportEvent> sportEvents) {
        Double rating = 0.0;
        Integer gamesPoints = 0;
        Integer min = 0;
        Integer max = 3;
        for (SportEvent sportEvent : sportEvents) {
            gamesPoints += sportEvent.getResultSet().getGuestTeamPoints();
        }
        rating = ((double) (gamesPoints - min) / (double) (max*sportEvents.size() - min));
        return rating.isNaN()? 0.0 : new BigDecimal(rating).setScale(4, RoundingMode.UP).doubleValue();
    }
}
