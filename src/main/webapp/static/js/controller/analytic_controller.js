'use strict';

App.controller('AnalyticController', ['$scope', 'AnalyticService', function($scope,AnalyticService){
    var self = this;
    self.parameters = [];
    self.calculateParams =  function(id){
        AnalyticService.calculateParams(id)
            .then(function(d){
                self.parameters = d;
            },
                function(errResponse){
                    console.error('Error while calculating coefficient')
                }
            )
    }
}]);