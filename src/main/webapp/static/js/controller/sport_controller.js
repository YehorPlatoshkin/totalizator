'use strict';

App.controller('SportController', ['$scope', 'SportService', function($scope, SportService) {
    var self = this;
    self.sport={id:null, sportName:'', analyticNetwork : null};
    self.sports=[];

    self.fetchAllSports = function(){
        SportService.fetchAllSports()
            .then(
                function(d) {
                    self.sports = d;
                },
                function(errResponse){
                    console.error('Error while fetching Sports');
                }
            );
    };

    self.createSport = function(sport){
        SportService.createSport(sport)
            .then(
                self.fetchAllSports,
                function(errResponse){
                    console.error('Error while creating sport.');
                }
            );
    };

    self.updateSport = function(sport){
        SportService.updateSport(sport)
            .then(
                self.fetchAllSports,
                function(errResponse){
                    console.error('Error while updating sport.');
                }
            );
    };

    self.fetchAllSports();

    self.submit = function() {
        if(self.sport.id==null){
            console.log('Saving New Sport', self.sport);
            self.createSport(self.sport);
        }else{
            self.updateSport(self.sport);
            console.log('Sport updated with id ', self.sport.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.sports.length; i++){
            if(self.sports[i].id == id) {
                self.sport = angular.copy(self.sports[i]);
                break;
            }
        }
    };

    self.reset = function(){
        self.sport={id:null,sportName:'',analyticNetwork : 'null'};
        $scope.sportForm.$setPristine(); //reset Form
    };
}]);

