'use strict';

App.controller('SportEventController',
    ['$scope', 'SportEventService', 'initialData',
        function ($scope, SportEventService, initialData, $window) {
            var self = this;
            self.sportEvent = {
                id: null,
                homeTeam: {id: null, team: {id: null, teamName: ''}},
                guestTeam: {id: null, team: {id: null, teamName: ''}},
                resultSet: {id: null, homeTeamPoints: null, guestTeamPoints: null},
                "coefficients": []
            };
            self.sportEvents = initialData;
            $scope.filteredEvents = [];
            self.pastEventsHT = [];
            self.pastEventsGT = [];
            $scope.currentPage = 1;
            $scope.numPerPage = 15;
            self.maxSize = 10;
            self.totalItems = self.sportEvents.length;

            self.fetchPastEventsHT = function (id) {
                SportEventService.pastEventsHT(id)
                    .then(function (d) {
                            self.pastEventsHT = d;
                        },
                        function (errResponse) {
                            console.error('Error while calculating coefficient')
                        })
            };
            self.fetchPastEventsGT = function (id) {
                SportEventService.pastEventsGT(id)
                    .then(function (d) {
                            self.pastEventsGT = d;
                        },
                        function (errResponse) {
                            console.error('Error while calculating coefficient')
                        })
            };
            self.fetchEventsByTeam = function (id) {
                SportEventService.fetchEventsByTeam(id)
                    .then(function (d) {
                            self.arr = d;
                        },
                        function (errResponse) {
                            console.error('Error while calculating coefficient')
                        }
                    )
            };
            self.calculateCoefficients = function (id) {
                SportEventService.calculateCoefficients(id)
                    .then(function () {
                            window.location.reload();
                        },
                        function (errResponse) {
                            console.error('Error while calculating coefficient')
                        }
                    )
            };
            self.parameters = [];
            self.calculateParams =  function(id){
                SportEventService.calculateParams(id)
                    .then(function(d){
                            self.parameters = d;
                        },
                        function(errResponse){
                            console.error('Error while calculating coefficient')
                        }
                    )
            };
            $scope.isCollapsed = false;
            $scope.$watch('currentPage + numPerPage', function () {
                var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    , end = begin + $scope.numPerPage;

                $scope.filteredEvents = self.sportEvents.slice(begin, end);
            });
        }]);

