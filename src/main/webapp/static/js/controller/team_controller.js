'use strict';

App.controller('TeamController', ['$scope', 'TeamService','teamInTournament', function($scope, TeamService, teamInTournament) {
    var self = this;
    self.teamT={"id": null,
        "tournament": null,
        "team": {
            "id": null,
            "teamName": ''
        },
        "sumPointsInTournament": null};
    self.teamTs= teamInTournament;
    $scope.filteredTeams = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 7;
    self.maxSize = 10;
    self.totalItems =self.teamTs.length;
    self.createTeam = function(teamT){
        TeamService.createTeam(teamT)
            .then(
/*
                self.fetchAllSports,
*/
                function(errResponse){
                    console.error('Error while creating team.');
                }
            );
    };

    self.updateTeam = function(teamT){
        TeamService.updateTeam(teamT)
            .then(
                /*self.fetchAllSports,*/
                function(errResponse){
                    console.error('Error while updating team.');
                }
            );
    };

    self.submit = function() {
        if(self.teamT.id==null){
            console.log('Saving New Team', self.teamT);
            self.createTeam(self.teamT);
        }else{
            self.updateTeam(self.teamT);
            console.log('Team updated with id ', self.teamT.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.teamTs.length; i++){
            if(self.teamTs[i].id == id) {
                self.teamT = angular.copy(self.teamTs[i]);
                break;
            }
        }
    };

    self.reset = function(){
        self.teamTs={"id": null,
            "tournament": null,
            "team": {
                "id": null,
                "teamName": ''
            },
            "sumPointsInTournament": null};
        $scope.teamForm.$setPristine(); //reset Form
    };
    $scope.$watch('currentPage + numPerPage', function() {
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
            , end = begin + $scope.numPerPage;

        $scope.filteredTeams = self.teamTs.slice(begin, end);
    });
}]);

