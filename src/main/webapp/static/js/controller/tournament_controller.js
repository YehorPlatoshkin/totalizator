'use strict';

App.controller('TournamentController', ['$scope', 'TournamentService','tournamentInSport', function($scope, TournamentService, tournamentInSport) {
    var self = this;
    self.tournament={id:null,tournamentName:'',dateTime:{year:null,monthOfYear:null, dayOfMonth:null},
        sport:null};
    self.tournaments = tournamentInSport;

    self.createTournament = function(tournament){
        TournamentService.createTournament(tournament)
            .then(
                /*self.fetchTournamentsBySport,*/
                self.tournaments = TournamentService.fetchTournamentsBySport(tournament.sport),
                function(errResponse){
                    console.error('Error while creating Tournament.');
                }
            );
    };

    self.updateTournament = function(tournament){
        TournamentService.updateTournament(tournament)
            .then(
/*
                self.tournaments = TournamentService.fetchTournamentsBySport(tournament.sport),
*/
                function(errResponse){
                    console.error('Error while updating sport.');
                }
            );
    };
    self.submit = function() {
        if(self.tournament.id==null){
            console.log('Saving New Tournament', self.tournament);
            self.createTournament(self.tournament);
        }else{
            self.updateTournament(self.tournament);
            console.log('tournament with id updated', self.tournament.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.tournaments.length; i++){
            if(self.tournaments[i].id == id) {
                self.tournament = angular.copy(self.tournaments[i]);
                break;
            }
        }
    };

    self.reset = function(){
        self.tournament={id:null,tournamentName:'',dateTime:{year:null,monthOfYear:null, dayOfMonth:null},
            sport:null};
        $scope.tournamentForm.$setPristine(); //reset Form
    };
}]);

/*    self.fetchTournamentsBySport = function(id){
 TournamentService.fetchTournamentsBySport(id)
 .then(
 function(d) {
 self.tournaments = d;
 },
 function(errResponse){
 console.error('Error while fetching Sports');
 }
 );
 };*/

/*
 self.fetchTournamentsBySport(id);
 */