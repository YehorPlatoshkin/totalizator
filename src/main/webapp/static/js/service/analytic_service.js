'use strict';

App.factory("AnalyticService", ['$http', '$q', function($http,$q){
    return {
        calculateParams: function(id){
            return $http.get('http://localhost:8080/analyticnetwork/'+id)
                .then(function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error calculation parameters');
                        return $q.reject(errResponse);
                    });
        }
    }
}]);