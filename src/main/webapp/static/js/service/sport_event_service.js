'use strict';

App.factory("SportEventService", ['$http', '$q', function ($http, $q) {
    return {
        fetchEventsBySport: function (id) {
            return $http.get('http://localhost:8080/sportevent/sport/' + id)
                .then(function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching sport events by sport');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchEventsByTournament: function (id) {
            return $http.get('http://localhost:8080/sportevent/tournament/' + id)
                .then(function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching sport events by tournament');
                        return $q.reject(errResponse);
                    }
                );
        },
        fetchEventsByTeam: function (id) {
            return $http.get('http://localhost:8080/sportevent/team/' + id)
                .then(function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching sport events by sport');
                        return $q.reject(errResponse);
                    }
                );
        },
        calculateCoefficients: function(id){
            return $http.post('http://localhost:8080/eventcoefficient/calc/'+id)
                .then(function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error calculation coefficient');
                        return $q.reject(errResponse);
                    });
        },
        pastEventsHT:function(id){
          return $http.get('http://localhost:8080/sportevent/pastEventsHT/'+id)
              .then(function(response){
                      return response.data;
                  },
                  function(errResponse){
                      console.error('Error calculation coefficient');
                      return $q.reject(errResponse);
                  });
        },
        pastEventsGT:function(id){
            return $http.get('http://localhost:8080/sportevent/pastEventsGT/'+id)
                .then(function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error calculation coefficient');
                        return $q.reject(errResponse);
                    });
        },
        getEvent:function(id){
            return $http.get('http://localhost:8080/sportevent/'+id)
                .then(function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while get event');
                    return $q.reject(errResponse);
                })
        },
        calculateParams: function(id){
            return $http.get('http://localhost:8080/analyticnetwork/'+id)
                .then(function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error calculation parameters');
                        return $q.reject(errResponse);
                    });
        }
        /*createSport: function (sport) {
            return $http.post('http://localhost:8080/sport', sport)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while creating sport');
                        return $q.reject(errResponse);
                    }
                );
        },
        updateSport: function (sport) {
            return $http.put('http://localhost:8080/sport', sport)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while updating user');
                        return $q.reject(errResponse);
                    }
                );
        }*/
    };
}]);

