'use strict';

App.factory("SportService", ['$http', '$q', function ($http, $q) {
    return {
        fetchAllSports: function () {
            return $http.get('http://localhost:8080/sport/all')
                .then(function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while fetching sport');
                        return $q.reject(errResponse);
                    }
                );
        },

        createSport: function (sport) {
            return $http.post('http://localhost:8080/sport', sport)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while creating sport');
                        return $q.reject(errResponse);
                    }
                );
        },
        updateSport: function (sport) {
            return $http.put('http://localhost:8080/sport', sport)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while updating user');
                        return $q.reject(errResponse);
                    }
                );
        }
    };
}]);

