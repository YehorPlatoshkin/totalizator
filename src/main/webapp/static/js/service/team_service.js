'use strict';

App.factory("TeamService", ['$http', '$q', function ($http, $q) {
    return {
        fetchTeamsByTournament: function (id) {
            return $http.get('http://localhost:8080/team/bytournament/' + id)
                .then(function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.log('Error while fetching tournament');
                        return $q.reject(errResponse);
                    }
                );
        },
        createTeam: function (team) {
            return $http.post('http://localhost:8080/team', team)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while creating tournament');
                        return $q.reject(errResponse);
                    }
                );
        },
        updateTeam: function (team) {
            return $http.put('http://localhost:8080/tournament/', team)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while updating user');
                        return $q.reject(errResponse);
                    }
                );
        }
    }
}]);
