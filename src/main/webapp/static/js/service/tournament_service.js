'use strict';

App.factory("TournamentService", ['$http', '$q', function ($http, $q) {
    return {
        fetchTournamentsBySport: function (id) {
            return $http.get('http://localhost:8080/tournament/bysport/' + id)
                .then(function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.log('Error while fetching tournament');
                        return $q.reject(errResponse);
                    }
                );
        },
        createTournament: function (tournament) {
            return $http.post('http://localhost:8080/tournament', tournament)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while creating tournament');
                        return $q.reject(errResponse);
                    }
                );
        },
        updateTournament: function (tournament) {
            return $http.put('http://localhost:8080/tournament/', tournament)
                .then(
                    function (response) {
                        return response.data;
                    },
                    function (errResponse) {
                        console.error('Error while updating tournament');
                        return $q.reject(errResponse);
                    }
                );
        }
    }
}]);
