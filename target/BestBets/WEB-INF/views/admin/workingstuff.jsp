<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Working Stuff</title>
    <link href="<c:url value="/static/css/bootstrap.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/static/css/app.css"/>" rel="stylesheet"/>
</head>
<body>
<div class="generic-container">
    <div class="panel panel-default">
        <%--Default panel contents--%>
        <div class="panel-heading"><span class="lead">List of Users</span></div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Full name</th>
                <th>E-mail</th>
                <th>State</th>
                <th>Role</th>
                <th width="100"></th>
                <th width="100"></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${workingstuff}" var="user">
                <tr>
                    <td>${user.fullName}</td>
                    <td>${user.email}</td>
                    <td>${user.state}</td>
                    <td>${user.profile.type}</td>
                    <td><a href="<c:url value="edit-user-${user.email}"/>"
                           class="btn btn-success custom-width">edit</a> </td>
                    <td><a href="<c:url value="delete-user-${user.email}"/> "
                           class="btn btn-danger custom-width"> delete</a> </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
