<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html">
    <title>Add sport event form</title>
    <link href="<c:url value="/static/css/bootstrap.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/static/css/app.css"/>" rel="stylesheet"/>
</head>
<body>
<div class="generic-container">
    <div class="well lead">Sport event create form</div>
    <form:form method="post" modelAttribute="sportevent" class="form-horizontal">
        <form:input type="hidden" path="id" id="id"/>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-label" for="eventBeginningDate">Event Beginning Date</label>

                <div class="col-md-7">
                    <form:input type="text" path="eventBeginningDate" id="eventBeginningDate" cssClass="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="eventBeginningDate" cssClass="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-label" for="homeTeam">Home Team</label>
                <div class="col-md-7">
                    <form:select path="homeTeam" items="${teams}" multiple="true" itemValue="id" itemLabel="teamName" cssClass="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="homeTeam" cssClass="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-label" for="homeTeam">Guest Team</label>
                <div class="col-md-7">
                    <form:select path="guestTeam" items="${teams}" multiple="true" itemValue="id" itemLabel="teamName" cssClass="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="guestTeam" cssClass="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row>">
            <div class="form-action floatRight">
                <input type="submit" value="Save and add more" class="btn btn-primary btn-sm"/>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>