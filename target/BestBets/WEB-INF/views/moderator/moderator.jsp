<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en" ng-app="bestBetsApp">
<head>

    <meta charset="utf-8">
    <title>Moderator cabinet</title>
    <style>
        .username.ng-valid {
            background-color: lightgreen;
        }
        .username.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .username.ng-dirty.ng-invalid-minlength {
            background-color: yellow;
        }

        .email.ng-valid {
            background-color: lightgreen;
        }
        .email.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .email.ng-dirty.ng-invalid-email {
            background-color: yellow;
        }

    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>
<body>
<div ng-controller="SportController as sportCtrl">

    <div class="row">
        <div class="col-lg-4">
            <div class="panel panel-default" ng-repeat="sport in sportCtrl.sports">
                <div class="panel-heading">
                    <h3 class="panel-title" ng-bind="sport.sportName"></h3>
                </div>
                <div class="panel-body">
                    <div ng-repeat="tournament in sport.tournaments">
                        <a href="#" data-open-team="true" data-tournament="{{tournament.id}}" data-sport="{{sport.id}}">
                            <span ng-bind="tournament.tournamentName"></span>
                            <span ng-bind="tournament.tournamentBeginningDate.year"></span>
                            <span ng-bind="tournament.tournamentBeginningDate.dayOfMonth"></span>
                            <span ng-bind="tournament.tournamentBeginningDate.monthOfYear"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">

            <div  ng-repeat="sport in sportCtrl.sports">
                <div class="panel panel-default" data-sport="{{sport.id}}">
                    <div ng-repeat="tournament in sport.tournaments" data-tournament="{{tournament.id}}" class="hide">
                        <div class="panel-heading">
                            <h3 class="panel-title" ng-bind="tournament.tournamentName"></h3>
                        </div>
                        <div class="panel-body">
                            <div ng-repeat="team in tournament.teams">
                                <span ng-bind="team.teamName"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-animate.js"></script>
<script src="<c:url value='/static/js/app.js' />"></script>
<script src="<c:url value='/static/js/bootstrap.js' />"></script>
<script src="<c:url value='/static/js/service/moderator_service.js' />"></script>
<script src="<c:url value='/static/js/controller/sport_controller.js' />"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.3.2.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            $('[data-open-team]').on('click', function () {
                var sportId = $(this).attr('data-sport');
                var tournamentId = $(this).attr('data-tournament');
                console.log(sportId, tournamentId);

                $('div[data-tournament]').addClass('hide');
                $('div[data-sport='+sportId+']').find('[data-tournament='+tournamentId+']').removeClass('hide');
            });
        }, 1000);
    });
</script>
</body>
</html>