<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Tournament List</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>
<div class="generic-container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of current events </span></div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Date of sport event</th>
                <th>Home Team</th>
                <th>Guest Team</th>
                <th width="100"></th>
                <th width="100"></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${present}" var="sportEvent">
                <tr>
                    <td>${sportEvent.eventBeginningDate}</td>
                    <td>${sportEvent.homeTeam}</td>
                    <td>${sportEvent.guestTeam}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of current events </span></div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Date of sport event</th>
                <th>Home Team</th>
                <th>Home Points</th>
                <th>Guest Points</th>
                <th width="100"></th>
                <th width="100"></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${past}" var="sportEvent">
                <tr>
                    <td>${sportEvent.eventBeginningDate}</td>
                    <td>${sportEvent.homeTeam}</td>
                    <td>${sportEvent.resultSet.homeTeamPoints}</td>
                    <td>${sportEvent.guestTeam}</td>
                    <td>${sportEvent.resultSet.guestTeamPoints}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="well">
        <a href="<c:url value='addsportevent-${tournamentId}' />">Add New Team</a>
    </div>
</div>
</body>
</html>