<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Tournament List</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>

<body>
<div class="generic-container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Tournaments </span></div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Tournament name</th>
                <th>Tournament Begining Date</th>

                <th width="100"></th>
                <th width="100"></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${tournaments}" var="tournament">
                <tr>
                    <td>${tournament.tournamentName}</td>
                    <td>${tournament.tournamentBeginningDate}</td>
                    <td><a href="<c:url value='teams-${tournament.id}' />" class="btn btn-success
 
custom-width">Team List</a></td>
                    <td><a href="<c:url value='sportevents-${tournament.id}' />" class="btn btn-danger
 
custom-width">Sport events List</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="well">
        <a href="<c:url value='addtournament-${sportName}' />">Add New Tournament</a>
    </div>
</div>
</body>
</html>