<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html">
    <title>User registration form</title>
    <link href="<c:url value="/static/css/bootstrap.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/static/css/app.css"/>" rel="stylesheet"/>
</head>
<body>
<div class="generic-container">
    <div class="well lead">User registration form</div>
    <form:form method="post" modelAttribute="user" class="form-horizontal">
        <form:input type="hidden" path="id" id="id"/>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-label" for="fullName">Full name</label>

                <div class="col-md-7">
                    <form:input type="text" path="fullName" id="fullName" cssClass="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="fullName" cssClass="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-label" for="email">E-mail</label>
                <c:choose>
                    <c:when test="${edit}">
                        <form:input type="text" path="email" id="email" cssClass="form-control input-sm"
                                    disabled="true"/>
                    </c:when>
                    <c:otherwise>
                        <form:input type="text" path="email" id="email" cssClass="form-control input-sm"/>
                        <div class="has-error">
                            <form:errors path="email" cssclass="help-inline"/>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control_table" for="password" >Password</label>
                <div class="col-md-7">
                    <form:input type="password" path="password" id="password" cssclass="form-control inpet-sm"/>
                    <div class="has-error">
                        <form:errors path="password" cssClass="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-label" for="profile">Roles</label>
                <div class="col-md-7">
                    <form:select path="profile" items="${roles}" multiple="true" itemValue="id" itemLabel="type" cssClass="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="profile" cssClass="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row>">
            <div class="form-action floatRight">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update" class="btn btn-primary btn-sm">
                        or <a href="<c:url value="/login"/> ">Cancel</a>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Register" class="btn btn-primary btn-sm">
                        or <a href="<c:url value="/login"/> ">Cancel</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>