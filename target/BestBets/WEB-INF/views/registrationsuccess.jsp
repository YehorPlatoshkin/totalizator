<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html">
    <title>Registration confirm page </title>
    <link href="<c:url value="/static/css/bootstrap.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/static/css/app.css"/>" rel="stylesheet"/>
</head>
<body>
    <div class="generic-container">
        <div class="alert alert-success lead">
            ${success}
        </div>
        <span class="well floatRight">
            Go to <a href="<c:url value="/login"/> ">Login</a>
        </span>
    </div>
</body>
</html>