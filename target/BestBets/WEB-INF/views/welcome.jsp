<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Welcome page</title>
</head>
<body>
Greeting : ${greeting}
This is a welcome page.
<div class="well">
    <a href="<c:url value="/registration"/>">registration</a>
</div>
<div class="well">
    <a href="<c:url value="/login"/>">Login</a>
</div>
</body>
</html>