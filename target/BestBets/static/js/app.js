'use strict';
var App = angular.module('bestBetsApp', ['ui.router', 'ngAnimate', 'ui.bootstrap']);

App.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('registration', {
            url: "/registration",
            templateUrl: 'registration',
            controller: "UserController as ctrl"
        })
        .state('login', {
            url: "/login",
            templateUrl: 'login',
            controller: "UserController as ctrl"
        })
        .state('admin', {
            url: "/admin",
            templateUrl: 'admin',
            controller: "UserController as ctrl"
        })
        .state('analytic', {
            url: "/analytic",
            templateUrl: 'analytic',
            controller: "UserController as ctrl"
        })
        .state('analytic.sport', {
            url: "/sports",
            templateUrl: 'sports',
            controller: "SportController as sportCtrl"
        })
        .state('analytic.tournament', {
            url: '/tournament{sportId:[0-9]{1,9}}',
            templateUrl: 'tournaments',
            controller: "TournamentController",
            controllerAs: "tournCtrl",
            resolve: {
                tournamentInSport: ['TournamentService', '$stateParams', function (TournamentService, $stateParams) {
                    return TournamentService.fetchTournamentsBySport($stateParams.sportId);
                }]
            }
        })
        .state('analytic.team', {
            url: '/team{tournamentId:[0-9]{1,9}}',//this url in the browser
            templateUrl: 'teams',//this url get template
            controller: "TeamController",
            controllerAs: "teamCtrl",
            resolve: {
                teamInTournament: ['TeamService', '$stateParams', function (TeamService, $stateParams) {
                    return TeamService.fetchTeamsByTournament($stateParams.tournamentId);//over here fetch content for view
                }]
            }
        })
        .state('analytic.sport.sportEvent', {
            url: "/sport{sportId:[0-9]{1,9}}",
            templateUrl: 'sportevents',
            controller: "SportEventController as sportECtrl",
            resolve: {
                initialData: ['SportEventService', '$stateParams', function (SportEventService, $stateParams) {
                    return SportEventService.fetchEventsBySport($stateParams.sportId);
                }]
            }
        })
        .state('analytic.tournament.sportEvent', {
            url: "/tournament{tournamentId:[0-9]{1,9}}",
            templateUrl: 'sportevents',
            controller: "SportEventController as sportECtrl",
            resolve: {
                initialData: ['SportEventService', '$stateParams', function (SportEventService, $stateParams) {
                    return SportEventService. fetchEventsByTournament($stateParams.tournamentId);
                }]
            }
        })
        .state('analytic.team.sportEvent', {
            url: "/team{teamId:[0-9]{1,9}}",
            templateUrl: 'sportevents',
            controller: "SportEventController as sportECtrl",
            resolve: {
                initialData: ['SportEventService', '$stateParams', function (SportEventService, $stateParams) {
                    return SportEventService.fetchEventsByTeam($stateParams.teamId);
                }]
            }
        })
        .state('analytic.sport.sportEvent.details', {
            url: "/details{sportEId:[0-9]{1,9}}",
            templateUrl: 'details',
            controller: "SportEventController as sportECtrl",
            resolve: {
                initialData: ['SportEventService', '$stateParams', function (SportEventService, $stateParams) {
                    return SportEventService.getEvent($stateParams.sportEId);
                }]
            }
        })
        /*.state('analytic.tournament.sportEvent.details', {
            url: "/tournament{tournamentId:[0-9]{1,9}}",
            templateUrl: 'sportevents',
            controller: "SportEventController as sportECtrl",
            resolve: {
                initialData: ['SportEventService', '$stateParams', function (SportEventService, $stateParams) {
                    return SportEventService. fetchEventsByTournament($stateParams.tournamentId);
                }]
            }
        })
        .state('analytic.team.sportEvent.details', {
            url: "/team{teamId:[0-9]{1,9}}",
            templateUrl: 'sportevents',
            controller: "SportEventController as sportECtrl",
            resolve: {
                initialData: ['SportEventService', '$stateParams', function (SportEventService, $stateParams) {
                    return SportEventService.fetchEventsByTeam($stateParams.teamId);
                }]
            }
        })*/;

    /*.state('analytic.tournament', {
     url:'/tournament{sportId:[0-9]{1,9}}',
     templateUrl : 'tournaments',
     controller: "TournamentController",
     controllerAs: "tournCtrl",
     resolve: {
     initialData : ['TournamentService', '$stateParams', function(TournamentService, $stateParams) {
     return TournamentService.fetchTournamentsBySport($stateParams.sportId) ;
     }]
     }
     })
     .state('analytic.team', {
     url:'/team{tournamentId:[0-9]{1,9}}',//this url in the browser
     templateUrl : 'teams',//this url get template
     controller: "TeamController",
     controllerAs: "teamCtrl",
     resolve: {
     initialData : ['TeamService', '$stateParams', function(TeamService, $stateParams) {
     return TeamService.fetchTeamsByTournament($stateParams.tournamentId) ;//over here fetch content for view
     }]
     }

     })*/
}]);
/*.state('analytic.sport', {
 url: '/{categoryId:[A-Za-z]{0,9}}',
 templateUrl: function(params){ return 'category/' + params.categoryId; },
 controller : "ItemListController as itemListCtrl",
 resolve: {
 async: ['ItemService', '$stateParams', function(ItemService, $stateParams) {
 return ItemService.fetchAllItems($stateParams.categoryId);
 }]
 }
 })

 .state('category.list.detail', {
 url: '/{itemId:[0-9]{1,9}}',
 templateUrl: function(params){ return 'category/' + params.categoryId +'/'+params.itemId; },
 controller : "ItemDetailsController as itemDetailsCtrl",
 resolve: {
 async: ['ItemService', '$stateParams', function(ItemService, $stateParams) {
 return ItemService.fetchSpecificItem($stateParams.categoryId, $stateParams.itemId);
 }]
 }
 })*/
/*
 App.config(['$routeProvider', function($routeProvider) {
 $routeProvider
 .when('/registration', {
 templateUrl: '/registration',
 controller : "UserController",
 controllerAs :  "ctrl"
 })
 .when('/login', {
 templateUrl: '/login',
 controller : "UserController"
 })
 .when('/admin', {
 templateUrl: '/admin',
 controller : "UserController",
 controllerAs :  "ctrl"
 })
 .when('/analytic', {
 templateUrl: '/analytic',
 controller : "UserController",
 controllerAs :  "ctrl"
 })
 .when('/sports', {
 templateUrl: '/sports',
 controller : "SportController",
 controllerAs :  "sportCtrl"
 })
 .when('/tournament/bysport/:id', {
 templateUrl: 'tournament/bysport',
 controller : "TournamentController as tournCtrl",
 resolve: {
 async: ['TournamentService','$route', function(TournamentService , $route) {
 return TournamentService.fetchTournamentsBySport($route.current.params.id);
 }]
 }
 })
 /!*
 .when('/items/printers', {
 templateUrl: 'items/printers',
 controller : "ItemListController as itemListCtrl",
 resolve: {
 async: ['ItemService', function(ItemService) {
 return ItemService.fetchAllItems('printers');
 }]
 }
 })
 .when('/items/computerdetails/:id', {
 templateUrl: 'items/computerdetails',
 controller : "ItemDetailsController as itemDetailsCtrl",
 resolve: {
 async: ['ItemService','$route', function(ItemService , $route) {
 return ItemService.fetchSpecificItem('computers',$route.current.params.id);
 }]
 }
 })
 .when('/items/phonedeailst/:id', {
 templateUrl: 'items/phonedetails',
 controller : "ItemDetailsController as itemDetailsCtrl",
 resolve: {
 async: ['ItemService','$route', function(ItemService , $route) {
 return ItemService.fetchSpecificItem('phones',$route.current.params.id);
 }]
 }
 })
 .when('/items/printerdetails/:id', {
 templateUrl: 'items/printerdetails',
 controller : "ItemDetailsController as itemDetailsCtrl",
 resolve: {
 async: ['ItemService','$route', function(ItemService , $route) {
 return ItemService.fetchSpecificItem('printers',$route.current.params.id);
 }]
 }
 })*!/

 .otherwise({redirectTo:'/'});
 }]);
 */
